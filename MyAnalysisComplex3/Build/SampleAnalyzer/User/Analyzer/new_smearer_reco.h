#ifndef NEW_SMEARER_H
#define NEW_SMEARER_H
// SampleAnalyzer headers
#include "SampleAnalyzer/Commons/DataFormat/EventFormat.h"
#include "SampleAnalyzer/Commons/DataFormat/SampleFormat.h"
#include "SampleAnalyzer/Commons/Base/SmearerBase.h"
namespace MA5
{
  class NewSmearer: public SmearerBase
  {
    public :
      /// Constructor without argument
      NewSmearer()
      {
          output_.Reset();
      }
      /// Destructor
      ~NewSmearer() {}

      /// Smearer methods: 
      /// Set parameters
      void SetParameters()
      {
          // Magnetic field along beam axis
          Bz_                 = 1.000000E-09;
          // Code-efficiency parameters
          ParticlePropagator_ = false;
          MuonSmearer_        = true;
          ElectronSmearer_    = true;
          PhotonSmearer_      = false;
          TauSmearer_         = true;
          JetSmearer_         = false;
      }

      /// Electron smearing method
      MCParticleFormat ElectronSmearer(const MCParticleFormat * part);

      /// Muon smearing method
      MCParticleFormat MuonSmearer(const MCParticleFormat * part);

      /// Hadronic Tau smearing method
      MCParticleFormat TauSmearer(const MCParticleFormat * part);

  };
}
#endif