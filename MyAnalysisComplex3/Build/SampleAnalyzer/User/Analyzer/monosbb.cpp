#include "SampleAnalyzer/User/Analyzer/monosbb.h"
#include "SampleAnalyzer/Commons/Service/LogService.h"
#include <fastjet/ClusterSequence.hh>
#include <fastjet/PseudoJet.hh>
#include <fastjet/contrib/VariableRPlugin.hh> 
#include <fastjet/tools/Filter.hh>
using namespace MA5;
using namespace std;

#define MATH_PI 3.141592
#define L_VERSION "3.2"
#pragma message ( "VERSION: " L_VERSION )

// -----------------------------------------------------------------------------
// Helpers
// function called before all
// -----------------------------------------------------------------------------
double monosbb::GetdPhi(double phi1, double phi2){
  double dphi = phi1 - phi2;
  if(fabs(dphi) > MATH_PI) dphi = (fabs(dphi)-2*MATH_PI);

  return fabs(dphi);
}
// Overlap Removal
template<typename T1, typename T2> vector<const T1*>
  Removal(vector<const T1*> &v1, vector<const T2*> &v2, const MAdouble64 &drmin)
{
  // Determining with objects should be removed
  vector<bool> mask(v1.size(),false);
  for (MAuint32 j=0;j<v1.size();j++)
    for (MAuint32 i=0;i<v2.size();i++)
      if (v2[i]->dr(v1[j]) < drmin)
      {
        mask[j]=true;
        break;
      }

  // Building the cleaned container
  vector<const T1*> cleaned_v1;
  for (MAuint32 i=0;i<v1.size();i++)
    if (!mask[i]) cleaned_v1.push_back(v1[i]);

  return cleaned_v1;
}
template<typename T1> vector<const T1*>
  RemovalLowPT(vector<const T1*> &v1, const MAdouble64 &drmin)
{
  // Determining with objects should be removed
  vector<bool> mask(v1.size(),false);
  for (MAuint32 j=0;j<v1.size();j++)
    for (MAuint32 i=0;i<v1.size();i++)
      if ((v1[i]->dr(v1[j]) < drmin) && (v1[i]->pt() > v1[j]->pt()))
      {
        mask[j]=true;
        break;
      }

  // Building the cleaned container
  vector<const T1*> cleaned_v1;
  for (MAuint32 i=0;i<v1.size();i++)
    if (!mask[i]) cleaned_v1.push_back(v1[i]);

  return cleaned_v1;
}
template<typename T1, typename T2> vector<const T1*>
  RemovalVariable1(vector<const T1*> &v1, vector<const T2*> &v2)
{
  // Determining with objects should be removed
  vector<bool> mask(v1.size(),false);
  for (MAuint32 j=0;j<v1.size();j++)
    for (MAuint32 i=0;i<v2.size();i++)
      {
        double drmin = min(0.4,0.4+10./v1[j]->pt());
        if (v2[i]->dr(v1[j]) < drmin)
      {
        mask[j]=true;
        break;
      }}

  // Building the cleaned container
  vector<const T1*> cleaned_v1;
  for (MAuint32 i=0;i<v1.size();i++)
    if (!mask[i]) cleaned_v1.push_back(v1[i]);

  return cleaned_v1;
}
//----------------------------------------------------------------------
/// a function that pretty prints a list of jets
void monosbb::print_jets (const fastjet::ClusterSequence & clust_seq,
                 const vector<fastjet::PseudoJet> & jets) {
  
  // sort jets into increasing pt
  vector<fastjet::PseudoJet> sorted_jets = sorted_by_pt(jets);
  
  // label the columns
  printf("%5s %10s %10s %10s %10s %10s %10s\n","jet #", "rapidity",
         "phi", "pt","m","e", "n constituents");
  
  // print out the details for each jet
  for (unsigned int i = 0; i < sorted_jets.size(); i++) {
    int n_constituents = clust_seq.constituents(sorted_jets[i]).size();
    printf("%5u %10.3f %10.3f %10.3f %10.3f %10.3f %8u\n",
           i, sorted_jets[i].rap(), sorted_jets[i].phi(),
           sorted_jets[i].perp(),sorted_jets[i].m(),sorted_jets[i].e(), n_constituents);
  }
}
// -----------------------------------------------------------------------------
// Initialize
// function called one time at the beginning of the analysis
// -----------------------------------------------------------------------------
bool monosbb::Initialize(const MA5::Configuration& cfg, const std::map<std::string,std::string>& parameters)
{
  cout << "BEGIN Initialization" << endl;
  //MA5::LogService::GetInstance()->SetVerbosityLevel(LogService::DEBUG_LEVEL);
  // initialize variables, histos
  // Information on the analysis, authors, ...
  // VERY IMPORTANT FOR DOCUMENTATION, TRACEABILITY, BUG REPORTS
  INFO << "        <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>" << endmsg;
  INFO << "        <>    Analysis: monosbb (SFS)                                         <>" << endmsg;
  INFO << "        <>    13 TeV, 139 fb^-1 luminosity                                    <>" << endmsg;
  INFO << "        <>    Recast by: Q.Liu                                                <>" << endmsg;
  INFO << "        <>    Contact: qibin.liu@cern.ch                                      <>" << endmsg;
  INFO << "        <>    Based on MadAnalysis 5 v1.8(substructure)                       <>" << endmsg;
  INFO << "        <>    For more information, see                                       <>" << endmsg;
  INFO << "        <>    http://madanalysis.irmp.ucl.ac.be/wiki/PublicAnalysisDatabase   <>" << endmsg;
  INFO << "        <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>" << endmsg;
  INFO << "        <><><><><><><><><><><><><><      v"<<L_VERSION<<"        ><><><><><><><><><><><><><>" << endmsg;

  // Initializing PhysicsService for MC
  PHYSICS->mcConfig().Reset();
  initialize_hardonic_def(PHYSICS);
  initialize_invisable_def(PHYSICS);
  // Initializing PhysicsService for RECO
  PHYSICS->recConfig().Reset();

  // Definition of the Signal Regions (SRs) of the analysis
  Manager()->AddRegionSelection("RESOLVED");
  Manager()->AddRegionSelection("MERGED");
  string COMBINED[] = {"RESOLVED","MERGED"};

  //Common
//better use ET binning region, but now we only have combined cutflow to validate
  Manager()->AddCut("Trigger_&_LeptonVeto");

//Resolved (j)
  Manager()->AddCut("RESOLVED_MET_cut", "RESOLVED");
  Manager()->AddCut("RESOLVED_nJet_cut", "RESOLVED");
  Manager()->AddCut("RESOLVED_nBJet_cut", "RESOLVED");
  Manager()->AddCut("RESOLVED_DijetMass_cut", "RESOLVED");
  Manager()->AddCut("RESOLVED_MassWindowLoose_cut", "RESOLVED");
  Manager()->AddCut("RESOLVED_min_dPhi_cut", "RESOLVED"); //min.dPhi(MET,Jet)_cut
  Manager()->AddCut("RESOLVED_S_cut", "RESOLVED"); //dummy now, sincethe significance not avilible if not fiting on-the-fly
  Manager()->AddCut("RESOLVED_DijetPT_cut", "RESOLVED");
  Manager()->AddCut("RESOLVED_mT_closetB", "RESOLVED");
  Manager()->AddCut("RESOLVED_mT_furthestB", "RESOLVED");

//Merged  (J&tj)
  Manager()->AddCut("MERGED_MET_cut", "MERGED");
  Manager()->AddCut("MERGED_nJet_cut", "MERGED");
  Manager()->AddCut("MERGED_nBJet_cut", "MERGED");
  Manager()->AddCut("MERGED_LJetMass_cut", "MERGED");
  Manager()->AddCut("MERGED_MassWindowLoose_cut", "MERGED");
  Manager()->AddCut("MERGED_nJetAsso_cut", "MERGED");
  Manager()->AddCut("MERGED_Trk_OR_cut", "MERGED");
  Manager()->AddCut("MERGED_min_dPhi_cut", "MERGED");

  Manager()->AddHisto("MET_resolved",7, 150,500, "RESOLVED");
  Manager()->AddHisto("MET_merged",4, 500,1500, "MERGED");
  Manager()->AddHisto("mjj",46, 50,280, "RESOLVED");
  Manager()->AddHisto("mJ",11, 50,270, "MERGED");
  Manager()->AddHisto("ptJ",100, 0,1000, "MERGED");

  Manager()->AddHisto("mjj_150_200ptv",46, 50,280, "RESOLVED");
  Manager()->AddHisto("mjj_200_350ptv",46, 50,280, "RESOLVED");
  Manager()->AddHisto("mjj_350_500ptv",46, 50,280, "RESOLVED");

  Manager()->AddHisto("mJ_500_750ptv",11, 50,270, "MERGED");
  Manager()->AddHisto("mJ_750ptv",11, 50,270, "MERGED");
/*  
  //debug distribution
  //resolved1: after METsig
  Manager()->AddHisto("plot1_ptjj",100, 0,1000, "RESOLVED");
  Manager()->AddHisto("plot1_ptj",100, 0,1000, "RESOLVED");
  Manager()->AddHisto("plot1_mjj",46, 50,280, "RESOLVED");
  Manager()->AddHisto("plot1_mj",100, 0,1000, "RESOLVED");
  //resolved2: after pt_jj cut
  Manager()->AddHisto("plot2_nj",100, 0,100, "RESOLVED");
  Manager()->AddHisto("plot2_nj_btagged",100, 0,100, "RESOLVED");
  Manager()->AddHisto("plot2_dPhi_bj_MET",100, -6.5,6.5, "RESOLVED");
  Manager()->AddHisto("plot2_mj",100, 0,1000, "RESOLVED");
  Manager()->AddHisto("plot2_mj_btagged",100, 100,1000, "RESOLVED");
  Manager()->AddHisto("plot2_ptj",100, 0.1000, "RESOLVED");
  Manager()->AddHisto("plot2_ptj_btagged",100, 0,1000, "RESOLVED");
  Manager()->AddHisto("plot2_mT",100, 0,1000, "RESOLVED");

  //merged1: after MET 500 cut
  Manager()->AddHisto("plot3_nTrkJet",100, 0,100, "MERGED");
  Manager()->AddHisto("plot3_nTrkJet_btagged",100, 0,100, "MERGED");
  Manager()->AddHisto("plot3_nVRJet",100, 0,100, "MERGED");
  Manager()->AddHisto("plot3_nVRJet_btagged",100, 0,100, "MERGED");

  Manager()->AddHisto("plot3a_nTrkJet",100, 0,100, "MERGED"); //asso
  Manager()->AddHisto("plot3a_nTrkJet_btagged",100, 0,100, "MERGED");
  Manager()->AddHisto("plot3a_nVRJet",100, 0,100, "MERGED");
  Manager()->AddHisto("plot3a_nVRJet_btagged",100, 0,100, "MERGED");

  //pt? m?
*/
  // //Fatjet, VR and GA debug 
  //fast-sim fatjet
  // Manager()->AddHisto("debug_ptJ",100, 0,1000, "MERGED");
  // Manager()->AddHisto("debug_mJ",100, 0,300, "MERGED");
  // Manager()->AddHisto("debug_etaJ",100, -4,4, "MERGED");
  // Manager()->AddHisto("debug_phiJ",100, -3.5,3.5, "MERGED");
  // Manager()->AddHisto("debug_nJ",10, 0,10, "MERGED");
  //recluster fatjet
  Manager()->AddHisto("debug_ptJ_new",100, 0,1000, "MERGED");
  Manager()->AddHisto("debug_mJ_new",100, 0,300, "MERGED");
  Manager()->AddHisto("debug_etaJ_new",100, -4,4, "MERGED");
  Manager()->AddHisto("debug_phiJ_new",100, -3.5,3.5, "MERGED");
  Manager()->AddHisto("debug_nJ_new",10, 0,10, "MERGED");
  // VR

  // kt Trk jet

  //asso




  //now load extra tagger for non-primary jets collection 
  tagger = new NewTagger(true);
  cout << "END   Initialization" << endl;
  return true;
}

// -----------------------------------------------------------------------------
// Finalize
// function called one time at the end of the analysis
// -----------------------------------------------------------------------------
void monosbb::Finalize(const SampleFormat& summary, const std::vector<SampleFormat>& files)
{
  cout << "BEGIN Finalization" << endl;
  // saving histos
  cout << "END   Finalization" << endl;
}

// -----------------------------------------------------------------------------
// Execute
// function called each time one event is read
// -----------------------------------------------------------------------------
bool monosbb::Execute(SampleFormat& sample, const EventFormat& event)
{
  // bool debug = false;
  if (event.rec() != 0)
  {

    double myEventWeight;
    if (Configuration().IsNoEventWeight())
      myEventWeight = 1.;
    else if (event.mc()->weight() != 0.)
      myEventWeight = event.mc()->weight();
    else
    {
      WARNING << "Found one event with a zero weight. Skipping..." << endmsg;
      return false;
    }
    Manager()->InitializeForNewEvent(myEventWeight);
    ///////////////////////// start
    //Defining the containers
    vector<const RecJetFormat *> SmallRJets;
    vector<const RecJetFormat *> SmallRJets_central;
    vector<const RecJetFormat *> SmallRJets_forward;


    // vector<RecJetFormat> KTTrkJets_modified; //id=trk_jet
    // vector<RecJetFormat *> KTTrkJets; //id=trk_jet
    vector<RecJetFormat> VRTrkJets_store; //re clustering
    vector<RecJetFormat *> VRTrkJets; //re clustering

    // vector<const RecJetFormat *> LargeRJets; //id=fat_jet


    vector<RecJetFormat> LargeRJets_new_store;
    vector<const RecJetFormat *> LargeRJets_new;
    
    vector<const RecJetFormat *> VRTrkJets_asso;
    // vector<const RecJetFormat *> VRTrkJets_dR_asso;
    // vector<const RecJetFormat *> KTTrkJets_dR_asso;
    // vector<const RecJetFormat *> SmallRJets_dR_asso;

    vector<const RecLeptonFormat *> Muons;
    vector<const RecLeptonFormat *> Electrons;
    vector<const RecTauFormat *> Taus;
    /////////////////////////////////////////// J&trkj ///////////////////////
    //extract everthing things...
    //we do not have any trimming/cleaning in the SFS version MA. Or you implement here.
/*
    //extract fat_jet
    for(auto const& imap: event.rec()->jetcollection())
      DEBUG << "DEBUG: id: " <<  imap.first <<endmsg;

    int leading_index = -1.;
    double max_pT = 0;
    if (event.rec()->jetcollection().count("fat_jet"))
    {
      const std::vector<RecJetFormat> &_FatJets = event.rec()->jets("fat_jet");
      for (unsigned int i = 0; i < _FatJets.size(); i++)
      {
        const RecJetFormat *this_fatjet = &(_FatJets[i]);
        if ((fabs(this_fatjet->eta()) >= 2.) || (this_fatjet->pt() < 200.))
          continue;
        LargeRJets.push_back(this_fatjet); //all selected jet
        if (leading_index < 0 || max_pT < this_fatjet->pt())
        {
          leading_index = i;
          max_pT = this_fatjet->pt();
        }
      }
      Manager()->FillHisto("debug_nJ", LargeRJets.size());
      if (LargeRJets.size() > 0)
      {
        SORTER->sort(LargeRJets, PTordering);
        Manager()->FillHisto("debug_ptJ", LargeRJets[0]->pt());
        Manager()->FillHisto("debug_etaJ", LargeRJets[0]->eta());
        Manager()->FillHisto("debug_phiJ", LargeRJets[0]->phi());
        Manager()->FillHisto("debug_mJ", LargeRJets[0]->m());
      }
    }

    if (LargeRJets.size() > 0)
    {
      if (fabs(LargeRJets[0]->eta()) >= 2.0)
        ERROR << "Error: Leading large-R jet have eta>=2.0, should not happen" << endmsg;
      if (leading_index == -1)
        ERROR << "Error: No large-R jet, should not happen " << endmsg;
      if (leading_index > 0)
      {
        DEBUG << "Warning: non-leading large-R seleted, first two after trimmer:" << endmsg;
        DEBUG << "1st pT = " << LargeRJets[0]->pt() << endmsg;
        if (LargeRJets.size() > 1)
          DEBUG << "2nd pT = " << LargeRJets[1]->pt() << endmsg;
      }
    }
*/  
  //############################################### recluster fatjet ####################
  //FatJet Reclustering
  std::vector<fastjet::PseudoJet> inputList;
  std::vector<fastjet::PseudoJet> outputList;
  // std::vector<fastjet::PseudoJet> trimmed_outputList;
  // vector<fastjet::PseudoJet>::iterator itOutputList;
  int hadron_index = 0;
  fastjet::PseudoJet jet_candidate;
  for (unsigned int i = 0; i < event.rec()->cluster_inputs().size(); i++)
  {
    jet_candidate = fastjet::PseudoJet(event.rec()->cluster_inputs()[i]);
    jet_candidate.set_user_index(hadron_index);
    inputList.push_back(jet_candidate);
    ++hadron_index;
  }
  fastjet::ClusterSequence sequence(inputList, fastjet::JetDefinition(fastjet::antikt_algorithm, 1.0));
  outputList = fastjet::sorted_by_pt(sequence.inclusive_jets(200.));
  // outputList = sequence.inclusive_jets(10.); //10GeV?
  LargeRJets_new_store.resize(outputList.size(),RecJetFormat());
  //triming and add constitunents
  fastjet::Filter trimmer(fastjet::JetDefinition(fastjet::kt_algorithm, 0.2), fastjet::SelectorPtFractionMin(0.05));
  // Manager()->FillHisto("nJ", outputList.size());
  for(unsigned int i = 0; i < outputList.size(); i++)
  {
    fastjet::PseudoJet& jet_formed = outputList.at(i); // refernence
    MALorentzVector momentum;
    momentum.SetPxPyPzE(jet_formed.px(), jet_formed.py(), jet_formed.pz(), jet_formed.E());
    RecJetFormat& fatjet = LargeRJets_new_store.at(i);
    // fatjet.setMomentum(momentum);
    //------------------------------------
    // Trimming 
    //------------------------------------ 
    fastjet::PseudoJet trimmed_jet = trimmer(jet_formed);
    // trimmed_jet = fastjet::join(trimmed_jet.constituents());
    // trimmed_outputList.push_back(trimmed_jet);
    momentum.SetPxPyPzE(trimmed_jet.px(), trimmed_jet.py(), trimmed_jet.pz(), trimmed_jet.E());
    fatjet.setMomentum(momentum);
    fatjet.setPseudoJet(trimmed_jet);
    // extra trim
    // candidate->TrimmedP4[0].SetPtEtaPhiM(trimmed_jet.pt(), trimmed_jet.eta(), trimmed_jet.phi(), trimmed_jet.m());
    // // four hardest subjets
    // subjets.clear();
    // subjets = trimmed_jet.pieces();
    // subjets = sorted_by_pt(subjets);
    // candidate->NSubJetsTrimmed = subjets.size();
    // for(size_t i = 0; i < subjets.size() and i < 4; i++)
    // {
    //   if(subjets.at(i).pt() < 0) continue;
    //   candidate->TrimmedP4[i + 1].SetPtEtaPhiM(subjets.at(i).pt(), subjets.at(i).eta(), subjets.at(i).phi(), subjets.at(i).m());
    // }
    // Manager()->FillHisto("ptJ", jet_formed.pt());
    // // DEBUG << "DEBUG: fatjet No. " << i <<endmsg;
    // // DEBUG << "DEBUG: outputList fatjet pt= " << jet_formed.pt() <<endmsg;
    // // DEBUG << "DEBUG: outputList fatjet m= " << jet_formed.m() <<endmsg;
    // Manager()->FillHisto("ptJ_trimmed_main", trimmed_jet.pt());
    // // DEBUG << "DEBUG: trimmed_outputList fatjet_mainjet_only pt= " << trimmed_jet.pt() <<endmsg; //since it is not joined with constitunnets, just main jet pT so smaller.
    // Manager()->FillHisto("mJ_trimmed_main", trimmed_jet.m());
    // // DEBUG << "DEBUG: trimmed_outputList fatjet_mainjet_only m= " << trimmed_jet.m() <<endmsg;
    // fastjet::PseudoJet trimmed_jet2 = fastjet::join(trimmed_jet.constituents());
    // Manager()->FillHisto("ptJ_trimmed_join", trimmed_jet2.pt());
    // // DEBUG << "DEBUG: trimmed_outputList fatjet_joined pt= " << trimmed_jet2.pt() <<endmsg; 
    // Manager()->FillHisto("mJ_trimmed_join", trimmed_jet2.m());
    // // DEBUG << "DEBUG: trimmed_outputList fatjet_joined m= " << trimmed_jet2.m() <<endmsg;
    // Manager()->FillHisto("etaJ", jet_formed.eta());
    // Manager()->FillHisto("phiJ", jet_formed.phi());
  } 
  // trimmed_outputList=fastjet::sorted_by_pt(trimmed_outputList); //for safe
  // ok, now we extraacte the fatjet.
  int leading_index=-1.;
  int max_pT=0;
  for(unsigned int i=0; i<LargeRJets_new_store.size(); i++){
    const RecJetFormat*  this_fatjet = &(LargeRJets_new_store[i]);
    if((fabs(this_fatjet->eta())>=2.) || (this_fatjet->pt() <200.)) continue;
    LargeRJets_new.push_back(this_fatjet); //all selected jet
    if(leading_index<0 || max_pT<this_fatjet->pt()) {
      leading_index=i;
      max_pT=this_fatjet->pt();
    }
  }
  Manager()->FillHisto("debug_nJ_new", LargeRJets_new.size());
  if (LargeRJets_new.size() > 0) {
    SORTER->sort(LargeRJets_new, PTordering);
    // for(unsigned int i=0; i<FatJets.size(); i++){
    //   INFO << "DEBUG: sorted fatjet pt= " << FatJets[i]->pt() <<endmsg;
    // }
    Manager()->FillHisto("debug_ptJ_new", LargeRJets_new[0]->pt());
    Manager()->FillHisto("debug_etaJ_new", LargeRJets_new[0]->eta());
    Manager()->FillHisto("debug_phiJ_new", LargeRJets_new[0]->phi());
    Manager()->FillHisto("debug_mJ_new", LargeRJets_new[0]->m());
  }
  if (LargeRJets_new.size() > 0)
  {
    if (fabs(LargeRJets_new[0]->eta())>=2.0) ERROR << "Error: Leading large-R jet have eta>=2.0, should not happen" << endmsg;
    if (leading_index==-1) ERROR << "Error: No large-R jet, should not happen " << endmsg;
    if (leading_index>0) {
      DEBUG << "Warning: non-leading large-R seleted, first two after trimmer:" << endmsg;
      DEBUG << "1st pT = " <<LargeRJets_new[0]->pt()<< endmsg;
      if(LargeRJets_new.size() > 1)
        DEBUG << "2nd pT = " <<LargeRJets_new[1]->pt()<< endmsg;
    }
  }
/*
    //###########################################################################################################################    
    //extract kt track jet
  if (event.rec()->jetcollection().count("trk_jet"))
  {
    const std::vector<RecJetFormat> &_TrkJets = event.rec()->jets("trk_jet");
    vector<const RecJetFormat *> _KTTrkJets; //id=trk_jet
    for (unsigned int i = 0; i < _TrkJets.size(); i++) //now we use main containter for VRtrkJet since the VR algo.
    {
      auto *this_trkJet = &(_TrkJets[i]);
      double pT = this_trkJet->momentum().Pt(), eta = this_trkJet->eta();
      if (pT > 10.0 && fabs(eta) < 2.5)
      {
        _KTTrkJets.push_back(this_trkJet);
      }
    }
    if (_KTTrkJets.size() > 0){
      KTTrkJets_modified.resize(_KTTrkJets.size(),RecJetFormat());
      for(unsigned int i = 0; i < _KTTrkJets.size(); i++){
        auto trkj_orig = _KTTrkJets.at(i);
        RecJetFormat& trkj = KTTrkJets_modified.at(i);
        trkj.setMomentum(trkj_orig->momentum());
        trkj.setPseudoJet(trkj_orig->pseudojet());
        for(auto contit:trkj_orig->constituents())
          trkj.AddConstituent(contit);
        KTTrkJets.push_back(&trkj);
      }
      SORTER->sort(KTTrkJets, PTordering);
      truth_matching(event,KTTrkJets);//match by quark... // order sensitive
      tagger->Execute(sample,event,KTTrkJets_modified);     //no order, so we just pass in the vector::object
    }
  }
*/
  //deal with VR trk jet
  double rho = 30.0;
  double min_r = 0.02;
  double max_r = 0.4;
  double ptmin = 10.0;
  
  fastjet::contrib::VariableRPlugin lvjet_pluginAKT(rho, min_r, max_r, fastjet::contrib::VariableRPlugin::AKTLIKE);
  fastjet::JetDefinition jet_defAKT(&lvjet_pluginAKT);
  const std::vector<fastjet::PseudoJet>& clust_inputs=event.rec()->cluster_inputs();
  // std::vector<fastjet::PseudoJet>& clust_inputs=LargeRJets[0].pseudojet().constituents() ;
  fastjet::ClusterSequence clust_seqAKT(clust_inputs, jet_defAKT);
  
  // tell the user what was done
  DEBUG << "# Ran " << jet_defAKT.description() << endmsg;
  
  // extract the inclusive jets with pt > 10 GeV
  vector<fastjet::PseudoJet> inclusive_jetsAKT = clust_seqAKT.inclusive_jets(ptmin);
  VRTrkJets_store.resize(inclusive_jetsAKT.size(),RecJetFormat());
  if(VRTrkJets_store.size()>0){
    for(unsigned int i = 0; i < inclusive_jetsAKT.size(); i++){
      fastjet::PseudoJet& j = inclusive_jetsAKT.at(i); // refernence
      MALorentzVector momentum;
      momentum.SetPxPyPzE(j.px(), j.py(), j.pz(), j.E());
      RecJetFormat& trkj = VRTrkJets_store.at(i);
      trkj.setMomentum(momentum);
      trkj.setPseudoJet(j);
      VRTrkJets.push_back(&trkj);
    }
  }
  if (VRTrkJets.size() > 0){
    SORTER->sort(VRTrkJets, PTordering);
    truth_matching(event,VRTrkJets);//match by quark... // order sensitive
    tagger->Execute(sample,event,VRTrkJets_store);     //no order, so we just pass in the vector::object
  }
    
  // print them out
  // DEBUG << "Printing inclusive jets with pt > "<< ptmin <<" GeV\n";
  // DEBUG << "---------------------------------------\n" ;
  // print_jets(clust_seqAKT, inclusive_jetsAKT);

    //do the GA and dR
    //use event.cluster_inputs() hadrons for entire event
    //jet.pseudojet(), raw pseduo jet from fastjet
    //jet.constituents(), MC particle

//GA VR
//switch to new fatjet
    if ((LargeRJets_new.size() > 0) && (VRTrkJets.size() > 0))
    {
      std::vector<fastjet::PseudoJet> pjs;
      for (auto tj : LargeRJets_new[0]->pseudojet().constituents()) //calo constituts
      {
        fastjet::PseudoJet pj = tj;
        pj.set_user_index(-1); // dummmy
        pjs.push_back(pj);
      }

      for (size_t i = 0; i < VRTrkJets.size(); ++i)
      {
        const RecJetFormat *trkjet = VRTrkJets.at(i);
        fastjet::PseudoJet pj = fastjet::PseudoJet(trkjet->px(), trkjet->py(), trkjet->pz(), trkjet->e());
        pj *= 1e-20; // ghostify momentum
        pj.set_user_index(i);
        pjs.push_back(pj);
      }

      fastjet::ClusterSequence reclustering(pjs, fastjet::JetDefinition(fastjet::antikt_algorithm, 1.0));
      std::vector<fastjet::PseudoJet> GAfatjet = fastjet::sorted_by_pt(reclustering.inclusive_jets(50.0)); //why 50?
      if (GAfatjet.size() > 0)
        DEBUG << "DEBUG: reclus. LargeRJet pt= " << GAfatjet[0].pt() << endmsg;
      for (auto pj : GAfatjet[0].constituents())
      {
        if (pj.user_index() >= 0)
        {
          const RecJetFormat *GAtrkjet = VRTrkJets.at(pj.user_index());
          VRTrkJets_asso.push_back(GAtrkjet);
          DEBUG << "DEBUG: GAtrkjet pt= " << GAtrkjet->pt() << endmsg;
          DEBUG << "DEBUG: GAtrkjet btag= " << GAtrkjet->btag() << endmsg;
          DEBUG << "DEBUG: GAtrkjet true_btag= " << GAtrkjet->true_btag() << endmsg;
          DEBUG << "DEBUG: GAtrkjet dR= " << GAtrkjet->dr(LargeRJets_new[0]) << endmsg;
        }
      }
      SORTER->sort(VRTrkJets_asso, PTordering);
      //plot somethngs to debug??!
/*
      //dR test
      for (size_t i = 0; i < VRTrkJets.size(); ++i)
      {
        const RecJetFormat *trkjet = VRTrkJets.at(i);
        if (trkjet->dr(LargeRJets_new[0]) <= 1.0)
        {
          VRTrkJets_dR_asso.push_back(trkjet);
          DEBUG << "DEBUG: trkjet pt= " << trkjet->pt() << endmsg;
          DEBUG << "DEBUG: trkjet btag= " << trkjet->btag() << endmsg;
          DEBUG << "DEBUG: trkjet true_btag= " << trkjet->true_btag() << endmsg;
          DEBUG << "DEBUG: trkjet mateched dR= " << trkjet->dr(LargeRJets_new[0]) << endmsg;
        }
      }

      //dR smallR
      for (size_t i = 0; i < SmallRJets.size(); ++i)
      {
        const RecJetFormat *this_jet = SmallRJets.at(i);
        if (this_jet->dr(LargeRJets_new[0]) <= 1.0)
        {
          SmallRJets_dR_asso.push_back(this_jet);
          DEBUG << "DEBUG: smallRJet pt= " << this_jet->pt() << endmsg;
          DEBUG << "DEBUG: smallRJet btag= " << this_jet->btag() << endmsg;
          DEBUG << "DEBUG: smallRJet true_btag= " << this_jet->true_btag() << endmsg;
          DEBUG << "DEBUG: smallRJet matched dR= " << this_jet->dr(LargeRJets_new[0]) << endmsg;
        }
      }

      //dR KTTrk
      for (size_t i = 0; i < KTTrkJets.size(); ++i)
      {
        const RecJetFormat *this_jet = KTTrkJets.at(i);
        if (this_jet->dr(LargeRJets_new[0]) <= 1.0)
        {
          KTTrkJets_dR_asso.push_back(this_jet);
          DEBUG << "DEBUG: KTTrkJets pt= " << this_jet->pt() << endmsg;
          DEBUG << "DEBUG: KTTrkJets btag= " << this_jet->btag() << endmsg;
          DEBUG << "DEBUG: KTTrkJets true_btag= " << this_jet->true_btag() << endmsg;
          DEBUG << "DEBUG: KTTrkJets matched dR= " << this_jet->dr(LargeRJets_new[0]) << endmsg;
        }
      }
*/
    DEBUG << "DEBUG: DONE GA VR" <<  endmsg;
    }
    /////////////////////////////////////////// j ///////////////////////
    const std::vector<RecJetFormat> &_SmallJets = event.rec()->jets();
    for (unsigned int i = 0; i < _SmallJets.size(); i++)
    {
      const RecJetFormat *this_jet = &(_SmallJets[i]);
      if (this_jet->pt() > 20.0 && fabs(this_jet->eta()) < 2.5)
      {
        SmallRJets_central.push_back(this_jet);
      }

      if (this_jet->pt() > 30.0 && fabs(this_jet->eta()) > 2.5 && fabs(this_jet->eta()) < 4.5)
      {
        SmallRJets_forward.push_back(this_jet);
      }
    }
    if (SmallRJets_central.size() > 0)
      SORTER->sort(SmallRJets_central, PTordering);
    if (SmallRJets_forward.size() > 0)
      SORTER->sort(SmallRJets_forward, PTordering);
    //fill special ordering j collection
    double pT_temp = -1;
    for (unsigned int i = 0; i < SmallRJets_central.size(); i++)
    {
      SmallRJets.push_back(SmallRJets_central.at(i));
      if ((pT_temp >= 0) && (pT_temp < SmallRJets_central.at(i)->pt())) //to check pT ordering...
        ERROR << "Error: not sorted central jets, should not happen" << endmsg;
      else
        pT_temp = SmallRJets_central.at(i)->pt();
    }
    pT_temp = -1;
    for (unsigned int i = 0; i < SmallRJets_forward.size(); i++)
    {
      SmallRJets.push_back(SmallRJets_forward.at(i));
      if ((pT_temp >= 0) && (pT_temp < SmallRJets_forward.at(i)->pt()))
        ERROR << "Error: not sorted forwardJets jets, should not happen" << endmsg;
      else
        pT_temp = SmallRJets_forward.at(i)->pt();
    }
    DEBUG << "DEBUG: small jets: " <<  SmallRJets.size() << endmsg;
    DEBUG << "DEBUG: central: " <<  SmallRJets_central.size() << endmsg;
    /////////////////////////////////////////// LEPMET ///////////////////////
    //Electrons: fast simulation...
    for (unsigned int i = 0; i < event.rec()->electrons().size(); i++)
    {
      const RecLeptonFormat *this_electron = &(event.rec()->electrons()[i]);
      if (this_electron->pt() > 7. && fabs(this_electron->eta()) < 2.47)
      {
        Electrons.push_back(this_electron); //SFS has no track...
      }
    }
    //Muons
    for (unsigned int i = 0; i < event.rec()->muons().size(); i++)
    {
      const RecLeptonFormat *this_muon = &(event.rec()->muons()[i]);
      if (this_muon->pt() > 7. && fabs(this_muon->eta()) < 2.7)
      {
        Muons.push_back(this_muon);
      }
    }
    //Taus, no hardonic tau... (which need tagging)
    for (unsigned int i = 0; i < event.rec()->taus().size(); i++)
    {
      const RecTauFormat *this_tau = &(event.rec()->taus()[i]);
      double eta = fabs(this_tau->eta());
      bool eta_pass = ((eta >= 1.52) && (eta < 2.5)) || (eta <= 1.37);
      if (this_tau->pt() > 20. && eta_pass)
      {
        Taus.push_back(this_tau);
        // tau-like j candidate is not consider here... (extended tau veto)
      }
      //Non-asso tau for merged region??
    }
    //MET
    MALorentzVector MET = (event.rec()->MET()).momentum();
    // MALorentzVector MET = (event.mc()->MET()).momentum();


    //we have no track, so that no TrkMiss...

    /////////////////////////////////////////// OVR ///////////////////////
    // ???
    //pileup??
    //trimming/cleaning jet??
    //isolation??

    //jetCleaning...
    //https://cds.cern.ch/record/2037702/files/ATLAS-CONF-2015-029.pdf
    // https://indico.cern.ch/event/631313/contributions/2683959/
    //https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HowToCleanJets2017
    // ???

    // Now test delphes UniqueObjetc OVR, not enable the analysis isolation/OVR
    // Now test delphes UniqueObjetc OVR, not enable the analysis isolation/OVR
    // //Overlap Removal
    // //e-e(track?), tau-e/mu, e-mu(track?), j-e, e-j, j-mu(special...TBD), mu-j, J-e
    Electrons = RemovalLowPT(Electrons, 0.01); //same track?
    Taus = Removal(Taus,Electrons, 0.2);
    Taus = Removal(Taus,Muons, 0.2);
    Electrons = Removal(Electrons, Muons, 0.01); //same track?
    SmallRJets = PHYSICS->Isol->JetCleaning(SmallRJets, Electrons, 0.2);
    SmallRJets_central = PHYSICS->Isol->JetCleaning(SmallRJets_central, Electrons, 0.2);
    SmallRJets_forward = PHYSICS->Isol->JetCleaning(SmallRJets_forward, Electrons, 0.2);
    Electrons = RemovalVariable1(Electrons,SmallRJets);
    SmallRJets = PHYSICS->Isol->JetCleaning(SmallRJets, Muons, 0.2); //should have extra condition: TBD...
    // //if the jets have fewer than three tracks
    // //or if the muon pT is greater than half the jet pT and greater than 70% of the pT sum of the tracks associated to the jet.
    SmallRJets_central = PHYSICS->Isol->JetCleaning(SmallRJets_central, Muons, 0.2);
    SmallRJets_forward = PHYSICS->Isol->JetCleaning(SmallRJets_forward, Muons, 0.2);
    Muons = RemovalVariable1(Muons,SmallRJets);
    LargeRJets_new = PHYSICS->Isol->JetCleaning(LargeRJets_new, Electrons, 0.1); //jetcleaning ok for fatjet?
    /////////////////////////////////////////// OK, Selection starts. ///////////////////////
    if (!Manager()->ApplyCut((MET.Pt() >= 110. && Muons.size() == 0 && Electrons.size() == 0 && Taus.size() == 0 ), "Trigger_&_LeptonVeto"))
      return true;
    if (MET.Pt() <= 500.)
    {
      Manager()->ApplyCut(false, "MERGED_MET_cut");
      //resolved
      if (!Manager()->ApplyCut((MET.Pt() > 150.)&&(MET.Pt() <=500.), "RESOLVED_MET_cut"))
        return true;

      DEBUG << "DEBUG: central: " << SmallRJets.size() <<" " << (SmallRJets_central.size() >= 2) << endmsg;
      if (!Manager()->ApplyCut((SmallRJets.size() >= 2), "RESOLVED_nJet_cut"))
        return true;
      //jj is constructed with so called H candidiate!
      // batgging efficiency must be corrected in delphs card according to new preformance papaer. (MV2c10 WP77)
      // Jets set: h candidates used in resolved region: first b then non-b, central, pT sorting in each cat.
      int NBJet = 0;
      vector<const RecJetFormat *> hJets;
      for (unsigned int i = 0; i < SmallRJets.size(); i++)
      {
        if (SmallRJets.at(i)->btag()) //bjet first
        {
          hJets.push_back(SmallRJets.at(i));
          NBJet++;
        }
      }
      for (unsigned int i = 0; i < SmallRJets.size(); i++)
      {
        if (!SmallRJets.at(i)->btag()) //then non-b jet
          hJets.push_back(SmallRJets.at(i));
      }
      MALorentzVector hCandidate4vec, hJets4vec[2]; //take first two
      for (unsigned int i = 0; i < 2; i++)
      {
        hJets4vec[i].SetPxPyPzE(hJets.at(i)->px(), hJets.at(i)->py(), hJets.at(i)->pz(), hJets.at(i)->e());
      }
      hCandidate4vec = hJets4vec[0] + hJets4vec[1];
      if (!Manager()->ApplyCut((NBJet >= 2), "RESOLVED_nBJet_cut"))
        return true;
      if (!Manager()->ApplyCut((hCandidate4vec.M() > 40.), "RESOLVED_DijetMass_cut"))
        return true;
      if (!Manager()->ApplyCut(((hCandidate4vec.M() > 50.) && (hCandidate4vec.M() < 280.)), "RESOLVED_MassWindowLoose_cut"))
        return true;
      //Multijet(QCD) suppress
      // Jets set: Central+Forward(That is the special order SmallRJets)
      // also check pT sorted
      bool mindPhiMETJets_big = true;
      int nJets=SmallRJets.size();
      for (unsigned int i = 0; i < min(3,nJets); i++)
      {
        // if (find(sorted_jet_pT.begin(), sorted_jet_pT.end(), centralJets.at(i)->pt()) >= sorted_jet_pT.begin() + 3)
        //   continue;
        double this_dPhi = GetdPhi(SmallRJets.at(i)->phi(), MET.Phi());
        if (this_dPhi < MATH_PI / 9)
        {
          mindPhiMETJets_big = false;
          break;
        }
      }
      if (!Manager()->ApplyCut((mindPhiMETJets_big), "RESOLVED_min_dPhi_cut"))
        return true;

      if (!Manager()->ApplyCut((true), "RESOLVED_S_cut"))
        return true; //dummy

      if (!Manager()->ApplyCut((hCandidate4vec.Pt() > 100.), "RESOLVED_DijetPT_cut"))
        return true;
      double max_dPhi = -1.;
      double max_dPhi_mT = -1.;
      double min_dPhi = -1;
      double min_dPhi_mT = -1.;
      for (unsigned int i = 0; i < SmallRJets_central.size(); i++)
      {
        if (!SmallRJets_central.at(i)->btag())
          continue;
        double this_dPhi = GetdPhi(SmallRJets_central.at(i)->phi(), MET.Phi());
        double pT = SmallRJets_central.at(i)->pt();
        double ET = MET.Pt();
        if (this_dPhi > max_dPhi)
        {
          max_dPhi = this_dPhi;
          max_dPhi_mT = sqrt(2 * pT * ET * (1 - cos(this_dPhi)));
          continue;
        }
        if ((this_dPhi < min_dPhi) || (min_dPhi < 0 && this_dPhi > 0))
        {
          min_dPhi = this_dPhi;
          min_dPhi_mT = sqrt(2 * pT * ET * (1 - cos(this_dPhi)));
          // min_dPhi_mT = PHYSICS->MT(SmallRJets.at(i),event);
          continue;
        }
      }
      if (!Manager()->ApplyCut((min_dPhi_mT > 170.), "RESOLVED_mT_closetB"))
        return true;
      if (!Manager()->ApplyCut((max_dPhi_mT > 200.), "RESOLVED_mT_furthestB"))
        return true;

      //Plotting
      Manager()->FillHisto("MET_resolved", MET.Pt());
      Manager()->FillHisto("mjj", hCandidate4vec.M());
      if(MET.Pt()>350)
        Manager()->FillHisto("mjj_350_500ptv", hCandidate4vec.M());
      else if(MET.Pt()>200)
        Manager()->FillHisto("mjj_200_350ptv", hCandidate4vec.M());
      else
        Manager()->FillHisto("mjj_150_200ptv", hCandidate4vec.M());
    }
    else
    {
      Manager()->ApplyCut(false, "RESOLVED_MET_cut");
      //merged
      if (!Manager()->ApplyCut((MET.Pt() > 500.), "MERGED_MET_cut"))
        return true;
      
      if (!Manager()->ApplyCut((LargeRJets_new.size() >= 1), "MERGED_nJet_cut"))
        return true;

      int NBTrk = 0;
      int NBTrk_asso= 0;
      // int NBTrk_dR_asso= 0;

      for (unsigned int i = 0; i < VRTrkJets.size(); i++) //VRTrkJets_asso?
      {
        if (VRTrkJets.at(i)->btag()) //bjet first
        {
          NBTrk++;
        }
      }

      for (unsigned int i = 0; i < VRTrkJets_asso.size(); i++) 
      {
        if (VRTrkJets_asso.at(i)->btag()) //bjet first
        {
          NBTrk_asso++;
        }
      }

      // for (unsigned int i = 0; i < VRTrkJets_dR_asso.size(); i++) 
      // {
      //   if (VRTrkJets_dR_asso.at(i)->btag()) //bjet first
      //   {
      //     NBTrk_dR_asso++;
      //   }
      // }

      if (!Manager()->ApplyCut((NBTrk_asso >= 2), "MERGED_nBJet_cut"))
        return true;

      if (!Manager()->ApplyCut((LargeRJets_new.at(0)->m() > 40.), "MERGED_LJetMass_cut"))
        return true;

      if (!Manager()->ApplyCut(((LargeRJets_new.at(0)->m() > 50.) && (LargeRJets_new.at(0)->m() < 270.)), "MERGED_MassWindowLoose_cut"))
        return true;

      if (!Manager()->ApplyCut((VRTrkJets_asso.size() >= 2), "MERGED_nJetAsso_cut"))
        return true;

      if (!Manager()->ApplyCut((true && true), "MERGED_Trk_OR_cut"))
        return true;

      //common small-R jet dPhi
      //common dphi MET small-R jet cut
      bool mindPhiMETJets_big = true;
      int nJets=SmallRJets.size();
      for (unsigned int i = 0; i < min(3,nJets); i++)
      {
        // if (find(sorted_jet_pT.begin(), sorted_jet_pT.end(), centralJets.at(i)->pt()) >= sorted_jet_pT.begin() + 3)
        //   continue;
        double this_dPhi = GetdPhi(SmallRJets.at(i)->phi(), MET.Phi());
        DEBUG << "DEBUG: dPhi_MET_j = " << this_dPhi << endmsg;
        if (this_dPhi < MATH_PI / 9)
        {
          mindPhiMETJets_big = false;
          break;
        }
      }
      if (!Manager()->ApplyCut((mindPhiMETJets_big), "MERGED_min_dPhi_cut"))
        return true;
      
      //Plotting
      Manager()->FillHisto("MET_merged", MET.Pt());
      Manager()->FillHisto("mJ", LargeRJets_new.at(0)->m());
      Manager()->FillHisto("ptJ", LargeRJets_new.at(0)->pt());
      if(MET.Pt()>750.)
        Manager()->FillHisto("mJ_750ptv", LargeRJets_new.at(0)->m());
      else
        Manager()->FillHisto("mJ_500_750ptv", LargeRJets_new.at(0)->m());
    }
  }
  return true;
}


