#ifndef NEW_TAGGER_H
#define NEW_TAGGER_H
// SampleAnalyzer headers
#include "SampleAnalyzer/Commons/DataFormat/EventFormat.h"
#include "SampleAnalyzer/Commons/DataFormat/SampleFormat.h"
namespace MA5
{
  class NewTagger
  {
    public :
      /// Constructor without argument
      NewTagger(bool _hack_mode=false) 
      {
        INFO <<"   <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>" << endmsg;
        INFO <<"   <>                                                              <>" << endmsg;
        INFO <<"   <>     Simplified Fast Detector Simulation in MadAnalysis 5     <>" << endmsg;
        INFO <<"   <>            Please cite arXiv:2006.09387 [hep-ph]             <>" << endmsg;
        INFO <<"   <>                                                              <>" << endmsg;
        INFO <<"   <>         https://madanalysis.irmp.ucl.ac.be/wiki/SFS          <>" << endmsg;
        INFO <<"   <>                                                              <>" << endmsg;
        INFO <<"   <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>" << endmsg;
        INFO <<"   Tagger for :" << ((_hack_mode) ? "(Hack mode: tagging extra jets collection)":"Primary jets") <<" is loaded!"<< endmsg;
      }

      /// Destructor
      virtual ~NewTagger() {}

      /// Tagger execution
      void Execute(SampleFormat &mySample, EventFormat &myEvent);
      void Execute(SampleFormat &mySample, const EventFormat &myEvent, std::vector<RecJetFormat>& jets);
  };
}
#endif