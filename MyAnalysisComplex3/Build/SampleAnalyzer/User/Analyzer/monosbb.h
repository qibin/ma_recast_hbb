#ifndef analysis_monosbb_h
#define analysis_monosbb_h

#include "SampleAnalyzer/Process/Analyzer/AnalyzerBase.h"
#include <fastjet/ClusterSequence.hh>

#include "new_smearer_reco.h"
#include "new_tagger.h"
namespace MA5
{
  class monosbb : public AnalyzerBase
  {
    INIT_ANALYSIS(monosbb, "monosbb")

  public:
    virtual bool Initialize(const MA5::Configuration &cfg, const std::map<std::string, std::string> &parameters);
    virtual void Finalize(const SampleFormat &summary, const std::vector<SampleFormat> &files);
    virtual bool Execute(SampleFormat &sample, const EventFormat &event);

  private:
    NewTagger* tagger;
    double GetdPhi(double phi1, double phi2);
    void print_jets(const fastjet::ClusterSequence &clust_seq,
                    const std::vector<fastjet::PseudoJet> &jets);
    void initialize_hardonic_def(PhysicsService *_PHYSICS)
    {
      // definition of the multiparticle "hadronic"
      _PHYSICS->mcConfig().AddHadronicId(-20543);
      _PHYSICS->mcConfig().AddHadronicId(-20533);
      _PHYSICS->mcConfig().AddHadronicId(-20523);
      _PHYSICS->mcConfig().AddHadronicId(-20513);
      _PHYSICS->mcConfig().AddHadronicId(-20433);
      _PHYSICS->mcConfig().AddHadronicId(-20423);
      _PHYSICS->mcConfig().AddHadronicId(-20413);
      _PHYSICS->mcConfig().AddHadronicId(-20323);
      _PHYSICS->mcConfig().AddHadronicId(-20313);
      _PHYSICS->mcConfig().AddHadronicId(-20213);
      _PHYSICS->mcConfig().AddHadronicId(-10543);
      _PHYSICS->mcConfig().AddHadronicId(-10541);
      _PHYSICS->mcConfig().AddHadronicId(-10533);
      _PHYSICS->mcConfig().AddHadronicId(-10531);
      _PHYSICS->mcConfig().AddHadronicId(-10523);
      _PHYSICS->mcConfig().AddHadronicId(-10521);
      _PHYSICS->mcConfig().AddHadronicId(-10513);
      _PHYSICS->mcConfig().AddHadronicId(-10511);
      _PHYSICS->mcConfig().AddHadronicId(-10433);
      _PHYSICS->mcConfig().AddHadronicId(-10431);
      _PHYSICS->mcConfig().AddHadronicId(-10423);
      _PHYSICS->mcConfig().AddHadronicId(-10421);
      _PHYSICS->mcConfig().AddHadronicId(-10413);
      _PHYSICS->mcConfig().AddHadronicId(-10411);
      _PHYSICS->mcConfig().AddHadronicId(-10323);
      _PHYSICS->mcConfig().AddHadronicId(-10321);
      _PHYSICS->mcConfig().AddHadronicId(-10313);
      _PHYSICS->mcConfig().AddHadronicId(-10311);
      _PHYSICS->mcConfig().AddHadronicId(-10213);
      _PHYSICS->mcConfig().AddHadronicId(-10211);
      _PHYSICS->mcConfig().AddHadronicId(-5554);
      _PHYSICS->mcConfig().AddHadronicId(-5544);
      _PHYSICS->mcConfig().AddHadronicId(-5542);
      _PHYSICS->mcConfig().AddHadronicId(-5534);
      _PHYSICS->mcConfig().AddHadronicId(-5532);
      _PHYSICS->mcConfig().AddHadronicId(-5524);
      _PHYSICS->mcConfig().AddHadronicId(-5522);
      _PHYSICS->mcConfig().AddHadronicId(-5514);
      _PHYSICS->mcConfig().AddHadronicId(-5512);
      _PHYSICS->mcConfig().AddHadronicId(-5503);
      _PHYSICS->mcConfig().AddHadronicId(-5444);
      _PHYSICS->mcConfig().AddHadronicId(-5442);
      _PHYSICS->mcConfig().AddHadronicId(-5434);
      _PHYSICS->mcConfig().AddHadronicId(-5432);
      _PHYSICS->mcConfig().AddHadronicId(-5424);
      _PHYSICS->mcConfig().AddHadronicId(-5422);
      _PHYSICS->mcConfig().AddHadronicId(-5414);
      _PHYSICS->mcConfig().AddHadronicId(-5412);
      _PHYSICS->mcConfig().AddHadronicId(-5403);
      _PHYSICS->mcConfig().AddHadronicId(-5401);
      _PHYSICS->mcConfig().AddHadronicId(-5342);
      _PHYSICS->mcConfig().AddHadronicId(-5334);
      _PHYSICS->mcConfig().AddHadronicId(-5332);
      _PHYSICS->mcConfig().AddHadronicId(-5324);
      _PHYSICS->mcConfig().AddHadronicId(-5322);
      _PHYSICS->mcConfig().AddHadronicId(-5314);
      _PHYSICS->mcConfig().AddHadronicId(-5312);
      _PHYSICS->mcConfig().AddHadronicId(-5303);
      _PHYSICS->mcConfig().AddHadronicId(-5301);
      _PHYSICS->mcConfig().AddHadronicId(-5242);
      _PHYSICS->mcConfig().AddHadronicId(-5232);
      _PHYSICS->mcConfig().AddHadronicId(-5224);
      _PHYSICS->mcConfig().AddHadronicId(-5222);
      _PHYSICS->mcConfig().AddHadronicId(-5214);
      _PHYSICS->mcConfig().AddHadronicId(-5212);
      _PHYSICS->mcConfig().AddHadronicId(-5203);
      _PHYSICS->mcConfig().AddHadronicId(-5201);
      _PHYSICS->mcConfig().AddHadronicId(-5142);
      _PHYSICS->mcConfig().AddHadronicId(-5132);
      _PHYSICS->mcConfig().AddHadronicId(-5122);
      _PHYSICS->mcConfig().AddHadronicId(-5114);
      _PHYSICS->mcConfig().AddHadronicId(-5112);
      _PHYSICS->mcConfig().AddHadronicId(-5103);
      _PHYSICS->mcConfig().AddHadronicId(-5101);
      _PHYSICS->mcConfig().AddHadronicId(-4444);
      _PHYSICS->mcConfig().AddHadronicId(-4434);
      _PHYSICS->mcConfig().AddHadronicId(-4432);
      _PHYSICS->mcConfig().AddHadronicId(-4424);
      _PHYSICS->mcConfig().AddHadronicId(-4422);
      _PHYSICS->mcConfig().AddHadronicId(-4414);
      _PHYSICS->mcConfig().AddHadronicId(-4412);
      _PHYSICS->mcConfig().AddHadronicId(-4403);
      _PHYSICS->mcConfig().AddHadronicId(-4334);
      _PHYSICS->mcConfig().AddHadronicId(-4332);
      _PHYSICS->mcConfig().AddHadronicId(-4324);
      _PHYSICS->mcConfig().AddHadronicId(-4322);
      _PHYSICS->mcConfig().AddHadronicId(-4314);
      _PHYSICS->mcConfig().AddHadronicId(-4312);
      _PHYSICS->mcConfig().AddHadronicId(-4303);
      _PHYSICS->mcConfig().AddHadronicId(-4301);
      _PHYSICS->mcConfig().AddHadronicId(-4232);
      _PHYSICS->mcConfig().AddHadronicId(-4224);
      _PHYSICS->mcConfig().AddHadronicId(-4222);
      _PHYSICS->mcConfig().AddHadronicId(-4214);
      _PHYSICS->mcConfig().AddHadronicId(-4212);
      _PHYSICS->mcConfig().AddHadronicId(-4203);
      _PHYSICS->mcConfig().AddHadronicId(-4201);
      _PHYSICS->mcConfig().AddHadronicId(-4132);
      _PHYSICS->mcConfig().AddHadronicId(-4122);
      _PHYSICS->mcConfig().AddHadronicId(-4114);
      _PHYSICS->mcConfig().AddHadronicId(-4112);
      _PHYSICS->mcConfig().AddHadronicId(-4103);
      _PHYSICS->mcConfig().AddHadronicId(-4101);
      _PHYSICS->mcConfig().AddHadronicId(-3334);
      _PHYSICS->mcConfig().AddHadronicId(-3324);
      _PHYSICS->mcConfig().AddHadronicId(-3322);
      _PHYSICS->mcConfig().AddHadronicId(-3314);
      _PHYSICS->mcConfig().AddHadronicId(-3312);
      _PHYSICS->mcConfig().AddHadronicId(-3303);
      _PHYSICS->mcConfig().AddHadronicId(-3224);
      _PHYSICS->mcConfig().AddHadronicId(-3222);
      _PHYSICS->mcConfig().AddHadronicId(-3214);
      _PHYSICS->mcConfig().AddHadronicId(-3212);
      _PHYSICS->mcConfig().AddHadronicId(-3203);
      _PHYSICS->mcConfig().AddHadronicId(-3201);
      _PHYSICS->mcConfig().AddHadronicId(-3122);
      _PHYSICS->mcConfig().AddHadronicId(-3114);
      _PHYSICS->mcConfig().AddHadronicId(-3112);
      _PHYSICS->mcConfig().AddHadronicId(-3103);
      _PHYSICS->mcConfig().AddHadronicId(-3101);
      _PHYSICS->mcConfig().AddHadronicId(-2224);
      _PHYSICS->mcConfig().AddHadronicId(-2214);
      _PHYSICS->mcConfig().AddHadronicId(-2212);
      _PHYSICS->mcConfig().AddHadronicId(-2203);
      _PHYSICS->mcConfig().AddHadronicId(-2114);
      _PHYSICS->mcConfig().AddHadronicId(-2112);
      _PHYSICS->mcConfig().AddHadronicId(-2103);
      _PHYSICS->mcConfig().AddHadronicId(-2101);
      _PHYSICS->mcConfig().AddHadronicId(-1114);
      _PHYSICS->mcConfig().AddHadronicId(-1103);
      _PHYSICS->mcConfig().AddHadronicId(-545);
      _PHYSICS->mcConfig().AddHadronicId(-543);
      _PHYSICS->mcConfig().AddHadronicId(-541);
      _PHYSICS->mcConfig().AddHadronicId(-535);
      _PHYSICS->mcConfig().AddHadronicId(-533);
      _PHYSICS->mcConfig().AddHadronicId(-531);
      _PHYSICS->mcConfig().AddHadronicId(-525);
      _PHYSICS->mcConfig().AddHadronicId(-523);
      _PHYSICS->mcConfig().AddHadronicId(-521);
      _PHYSICS->mcConfig().AddHadronicId(-515);
      _PHYSICS->mcConfig().AddHadronicId(-513);
      _PHYSICS->mcConfig().AddHadronicId(-511);
      _PHYSICS->mcConfig().AddHadronicId(-435);
      _PHYSICS->mcConfig().AddHadronicId(-433);
      _PHYSICS->mcConfig().AddHadronicId(-431);
      _PHYSICS->mcConfig().AddHadronicId(-425);
      _PHYSICS->mcConfig().AddHadronicId(-423);
      _PHYSICS->mcConfig().AddHadronicId(-421);
      _PHYSICS->mcConfig().AddHadronicId(-415);
      _PHYSICS->mcConfig().AddHadronicId(-413);
      _PHYSICS->mcConfig().AddHadronicId(-411);
      _PHYSICS->mcConfig().AddHadronicId(-325);
      _PHYSICS->mcConfig().AddHadronicId(-323);
      _PHYSICS->mcConfig().AddHadronicId(-321);
      _PHYSICS->mcConfig().AddHadronicId(-315);
      _PHYSICS->mcConfig().AddHadronicId(-313);
      _PHYSICS->mcConfig().AddHadronicId(-311);
      _PHYSICS->mcConfig().AddHadronicId(-215);
      _PHYSICS->mcConfig().AddHadronicId(-213);
      _PHYSICS->mcConfig().AddHadronicId(-211);
      _PHYSICS->mcConfig().AddHadronicId(111);
      _PHYSICS->mcConfig().AddHadronicId(113);
      _PHYSICS->mcConfig().AddHadronicId(115);
      _PHYSICS->mcConfig().AddHadronicId(130);
      _PHYSICS->mcConfig().AddHadronicId(211);
      _PHYSICS->mcConfig().AddHadronicId(213);
      _PHYSICS->mcConfig().AddHadronicId(215);
      _PHYSICS->mcConfig().AddHadronicId(221);
      _PHYSICS->mcConfig().AddHadronicId(223);
      _PHYSICS->mcConfig().AddHadronicId(225);
      _PHYSICS->mcConfig().AddHadronicId(310);
      _PHYSICS->mcConfig().AddHadronicId(311);
      _PHYSICS->mcConfig().AddHadronicId(313);
      _PHYSICS->mcConfig().AddHadronicId(315);
      _PHYSICS->mcConfig().AddHadronicId(321);
      _PHYSICS->mcConfig().AddHadronicId(323);
      _PHYSICS->mcConfig().AddHadronicId(325);
      _PHYSICS->mcConfig().AddHadronicId(331);
      _PHYSICS->mcConfig().AddHadronicId(333);
      _PHYSICS->mcConfig().AddHadronicId(335);
      _PHYSICS->mcConfig().AddHadronicId(411);
      _PHYSICS->mcConfig().AddHadronicId(413);
      _PHYSICS->mcConfig().AddHadronicId(415);
      _PHYSICS->mcConfig().AddHadronicId(421);
      _PHYSICS->mcConfig().AddHadronicId(423);
      _PHYSICS->mcConfig().AddHadronicId(425);
      _PHYSICS->mcConfig().AddHadronicId(431);
      _PHYSICS->mcConfig().AddHadronicId(433);
      _PHYSICS->mcConfig().AddHadronicId(435);
      _PHYSICS->mcConfig().AddHadronicId(441);
      _PHYSICS->mcConfig().AddHadronicId(443);
      _PHYSICS->mcConfig().AddHadronicId(445);
      _PHYSICS->mcConfig().AddHadronicId(511);
      _PHYSICS->mcConfig().AddHadronicId(513);
      _PHYSICS->mcConfig().AddHadronicId(515);
      _PHYSICS->mcConfig().AddHadronicId(521);
      _PHYSICS->mcConfig().AddHadronicId(523);
      _PHYSICS->mcConfig().AddHadronicId(525);
      _PHYSICS->mcConfig().AddHadronicId(531);
      _PHYSICS->mcConfig().AddHadronicId(533);
      _PHYSICS->mcConfig().AddHadronicId(535);
      _PHYSICS->mcConfig().AddHadronicId(541);
      _PHYSICS->mcConfig().AddHadronicId(543);
      _PHYSICS->mcConfig().AddHadronicId(545);
      _PHYSICS->mcConfig().AddHadronicId(551);
      _PHYSICS->mcConfig().AddHadronicId(553);
      _PHYSICS->mcConfig().AddHadronicId(555);
      _PHYSICS->mcConfig().AddHadronicId(1103);
      _PHYSICS->mcConfig().AddHadronicId(1114);
      _PHYSICS->mcConfig().AddHadronicId(2101);
      _PHYSICS->mcConfig().AddHadronicId(2103);
      _PHYSICS->mcConfig().AddHadronicId(2112);
      _PHYSICS->mcConfig().AddHadronicId(2114);
      _PHYSICS->mcConfig().AddHadronicId(2203);
      _PHYSICS->mcConfig().AddHadronicId(2212);
      _PHYSICS->mcConfig().AddHadronicId(2214);
      _PHYSICS->mcConfig().AddHadronicId(2224);
      _PHYSICS->mcConfig().AddHadronicId(3101);
      _PHYSICS->mcConfig().AddHadronicId(3103);
      _PHYSICS->mcConfig().AddHadronicId(3112);
      _PHYSICS->mcConfig().AddHadronicId(3114);
      _PHYSICS->mcConfig().AddHadronicId(3122);
      _PHYSICS->mcConfig().AddHadronicId(3201);
      _PHYSICS->mcConfig().AddHadronicId(3203);
      _PHYSICS->mcConfig().AddHadronicId(3212);
      _PHYSICS->mcConfig().AddHadronicId(3214);
      _PHYSICS->mcConfig().AddHadronicId(3222);
      _PHYSICS->mcConfig().AddHadronicId(3224);
      _PHYSICS->mcConfig().AddHadronicId(3303);
      _PHYSICS->mcConfig().AddHadronicId(3312);
      _PHYSICS->mcConfig().AddHadronicId(3314);
      _PHYSICS->mcConfig().AddHadronicId(3322);
      _PHYSICS->mcConfig().AddHadronicId(3324);
      _PHYSICS->mcConfig().AddHadronicId(3334);
      _PHYSICS->mcConfig().AddHadronicId(4101);
      _PHYSICS->mcConfig().AddHadronicId(4103);
      _PHYSICS->mcConfig().AddHadronicId(4112);
      _PHYSICS->mcConfig().AddHadronicId(4114);
      _PHYSICS->mcConfig().AddHadronicId(4122);
      _PHYSICS->mcConfig().AddHadronicId(4132);
      _PHYSICS->mcConfig().AddHadronicId(4201);
      _PHYSICS->mcConfig().AddHadronicId(4203);
      _PHYSICS->mcConfig().AddHadronicId(4212);
      _PHYSICS->mcConfig().AddHadronicId(4214);
      _PHYSICS->mcConfig().AddHadronicId(4222);
      _PHYSICS->mcConfig().AddHadronicId(4224);
      _PHYSICS->mcConfig().AddHadronicId(4232);
      _PHYSICS->mcConfig().AddHadronicId(4301);
      _PHYSICS->mcConfig().AddHadronicId(4303);
      _PHYSICS->mcConfig().AddHadronicId(4312);
      _PHYSICS->mcConfig().AddHadronicId(4314);
      _PHYSICS->mcConfig().AddHadronicId(4322);
      _PHYSICS->mcConfig().AddHadronicId(4324);
      _PHYSICS->mcConfig().AddHadronicId(4332);
      _PHYSICS->mcConfig().AddHadronicId(4334);
      _PHYSICS->mcConfig().AddHadronicId(4403);
      _PHYSICS->mcConfig().AddHadronicId(4412);
      _PHYSICS->mcConfig().AddHadronicId(4414);
      _PHYSICS->mcConfig().AddHadronicId(4422);
      _PHYSICS->mcConfig().AddHadronicId(4424);
      _PHYSICS->mcConfig().AddHadronicId(4432);
      _PHYSICS->mcConfig().AddHadronicId(4434);
      _PHYSICS->mcConfig().AddHadronicId(4444);
      _PHYSICS->mcConfig().AddHadronicId(5101);
      _PHYSICS->mcConfig().AddHadronicId(5103);
      _PHYSICS->mcConfig().AddHadronicId(5112);
      _PHYSICS->mcConfig().AddHadronicId(5114);
      _PHYSICS->mcConfig().AddHadronicId(5122);
      _PHYSICS->mcConfig().AddHadronicId(5132);
      _PHYSICS->mcConfig().AddHadronicId(5142);
      _PHYSICS->mcConfig().AddHadronicId(5201);
      _PHYSICS->mcConfig().AddHadronicId(5203);
      _PHYSICS->mcConfig().AddHadronicId(5212);
      _PHYSICS->mcConfig().AddHadronicId(5214);
      _PHYSICS->mcConfig().AddHadronicId(5222);
      _PHYSICS->mcConfig().AddHadronicId(5224);
      _PHYSICS->mcConfig().AddHadronicId(5232);
      _PHYSICS->mcConfig().AddHadronicId(5242);
      _PHYSICS->mcConfig().AddHadronicId(5301);
      _PHYSICS->mcConfig().AddHadronicId(5303);
      _PHYSICS->mcConfig().AddHadronicId(5312);
      _PHYSICS->mcConfig().AddHadronicId(5314);
      _PHYSICS->mcConfig().AddHadronicId(5322);
      _PHYSICS->mcConfig().AddHadronicId(5324);
      _PHYSICS->mcConfig().AddHadronicId(5332);
      _PHYSICS->mcConfig().AddHadronicId(5334);
      _PHYSICS->mcConfig().AddHadronicId(5342);
      _PHYSICS->mcConfig().AddHadronicId(5401);
      _PHYSICS->mcConfig().AddHadronicId(5403);
      _PHYSICS->mcConfig().AddHadronicId(5412);
      _PHYSICS->mcConfig().AddHadronicId(5414);
      _PHYSICS->mcConfig().AddHadronicId(5422);
      _PHYSICS->mcConfig().AddHadronicId(5424);
      _PHYSICS->mcConfig().AddHadronicId(5432);
      _PHYSICS->mcConfig().AddHadronicId(5434);
      _PHYSICS->mcConfig().AddHadronicId(5442);
      _PHYSICS->mcConfig().AddHadronicId(5444);
      _PHYSICS->mcConfig().AddHadronicId(5503);
      _PHYSICS->mcConfig().AddHadronicId(5512);
      _PHYSICS->mcConfig().AddHadronicId(5514);
      _PHYSICS->mcConfig().AddHadronicId(5522);
      _PHYSICS->mcConfig().AddHadronicId(5524);
      _PHYSICS->mcConfig().AddHadronicId(5532);
      _PHYSICS->mcConfig().AddHadronicId(5534);
      _PHYSICS->mcConfig().AddHadronicId(5542);
      _PHYSICS->mcConfig().AddHadronicId(5544);
      _PHYSICS->mcConfig().AddHadronicId(5554);
      _PHYSICS->mcConfig().AddHadronicId(10111);
      _PHYSICS->mcConfig().AddHadronicId(10113);
      _PHYSICS->mcConfig().AddHadronicId(10211);
      _PHYSICS->mcConfig().AddHadronicId(10213);
      _PHYSICS->mcConfig().AddHadronicId(10221);
      _PHYSICS->mcConfig().AddHadronicId(10223);
      _PHYSICS->mcConfig().AddHadronicId(10311);
      _PHYSICS->mcConfig().AddHadronicId(10313);
      _PHYSICS->mcConfig().AddHadronicId(10321);
      _PHYSICS->mcConfig().AddHadronicId(10323);
      _PHYSICS->mcConfig().AddHadronicId(10331);
      _PHYSICS->mcConfig().AddHadronicId(10333);
      _PHYSICS->mcConfig().AddHadronicId(10411);
      _PHYSICS->mcConfig().AddHadronicId(10413);
      _PHYSICS->mcConfig().AddHadronicId(10421);
      _PHYSICS->mcConfig().AddHadronicId(10423);
      _PHYSICS->mcConfig().AddHadronicId(10431);
      _PHYSICS->mcConfig().AddHadronicId(10433);
      _PHYSICS->mcConfig().AddHadronicId(10441);
      _PHYSICS->mcConfig().AddHadronicId(10443);
      _PHYSICS->mcConfig().AddHadronicId(10511);
      _PHYSICS->mcConfig().AddHadronicId(10513);
      _PHYSICS->mcConfig().AddHadronicId(10521);
      _PHYSICS->mcConfig().AddHadronicId(10523);
      _PHYSICS->mcConfig().AddHadronicId(10531);
      _PHYSICS->mcConfig().AddHadronicId(10533);
      _PHYSICS->mcConfig().AddHadronicId(10541);
      _PHYSICS->mcConfig().AddHadronicId(10543);
      _PHYSICS->mcConfig().AddHadronicId(10551);
      _PHYSICS->mcConfig().AddHadronicId(10553);
      _PHYSICS->mcConfig().AddHadronicId(20113);
      _PHYSICS->mcConfig().AddHadronicId(20213);
      _PHYSICS->mcConfig().AddHadronicId(20223);
      _PHYSICS->mcConfig().AddHadronicId(20313);
      _PHYSICS->mcConfig().AddHadronicId(20323);
      _PHYSICS->mcConfig().AddHadronicId(20333);
      _PHYSICS->mcConfig().AddHadronicId(20413);
      _PHYSICS->mcConfig().AddHadronicId(20423);
      _PHYSICS->mcConfig().AddHadronicId(20433);
      _PHYSICS->mcConfig().AddHadronicId(20443);
      _PHYSICS->mcConfig().AddHadronicId(20513);
      _PHYSICS->mcConfig().AddHadronicId(20523);
      _PHYSICS->mcConfig().AddHadronicId(20533);
      _PHYSICS->mcConfig().AddHadronicId(20543);
      _PHYSICS->mcConfig().AddHadronicId(20553);
      _PHYSICS->mcConfig().AddHadronicId(100443);
      _PHYSICS->mcConfig().AddHadronicId(100553);
      _PHYSICS->mcConfig().AddHadronicId(9900440);
      _PHYSICS->mcConfig().AddHadronicId(9900441);
      _PHYSICS->mcConfig().AddHadronicId(9900443);
      _PHYSICS->mcConfig().AddHadronicId(9900551);
      _PHYSICS->mcConfig().AddHadronicId(9900553);
      _PHYSICS->mcConfig().AddHadronicId(9910441);
      _PHYSICS->mcConfig().AddHadronicId(9910551);
    };
    void initialize_invisable_def(PhysicsService *_PHYSICS)
    {
      // definition of the multiparticle "invisible"
      _PHYSICS->mcConfig().AddInvisibleId(-16);
      _PHYSICS->mcConfig().AddInvisibleId(-14);
      _PHYSICS->mcConfig().AddInvisibleId(-12);
      _PHYSICS->mcConfig().AddInvisibleId(12);
      _PHYSICS->mcConfig().AddInvisibleId(14);
      _PHYSICS->mcConfig().AddInvisibleId(16);
      _PHYSICS->mcConfig().AddInvisibleId(1000022);
      _PHYSICS->mcConfig().AddInvisibleId(1000039);
    };

    //btaggeing method 1: matching b quark then assign efficiency
    void truth_matching(const EventFormat &myEvent, std::vector<RecJetFormat* > jets)
    {
      MAfloat64 DeltaRmax_ = 0.3;
      MAbool Exclusive_ = true;

      std::vector<RecJetFormat *> Candidates;

      // Matching b-quarks to jets
      for (MAuint32 i = 0; i < myEvent.rec()->MCBquarks().size(); i++)
      {
        RecJetFormat *tag = 0;
        MAfloat64 DeltaRmax = DeltaRmax_;

        // loop on the jets
        for (MAuint32 j = 0; j < jets.size(); j++)
        {
          if (jets[j]->pt() < 1e-10)
            continue;
          MAfloat32 DeltaR = myEvent.rec()->MCBquarks()[i]->dr(jets[j]);

          if (DeltaR <= DeltaRmax)
          {
            if (Exclusive_)
            {
              tag = jets[j];
              DeltaRmax = DeltaR;
            }
            else
              Candidates.push_back(jets[j]);
          }
        }
        if (Exclusive_ && tag != 0)
          Candidates.push_back(tag);
      }

      // Tagging the b-jet
      for (MAuint32 i = 0; i < Candidates.size(); i++)
      {
        Candidates[i]->setTrueBtag(true);
      }
      Candidates.clear();

      // Matching c-quarks to jets
      for (MAuint32 i = 0; i < myEvent.rec()->MCCquarks().size(); i++)
      {
        RecJetFormat *tag = 0;
        MAfloat64 DeltaRmax = DeltaRmax_;

        // loop on the jets
        for (MAuint32 j = 0; j < jets.size(); j++)
        {
          if (jets[j]->pt() < 1e-10)
            continue;

          MAfloat32 DeltaR =
              myEvent.rec()->MCCquarks()[i]->dr(jets[j]);

          if (DeltaR <= DeltaRmax)
          {
            if (Exclusive_)
            {
              tag = jets[j];
              DeltaRmax = DeltaR;
            }
            else
              Candidates.push_back(jets[j]);
          }
        }
        if (Exclusive_ && tag != 0)
          Candidates.push_back(tag);
      }

      // Tagging the c-jet
      for (MAuint32 i = 0; i < Candidates.size(); i++)
      {
        if (Candidates[i]->true_btag())
          continue;
        Candidates[i]->setTrueCtag(true);
      }
    };
  };
}

#endif
