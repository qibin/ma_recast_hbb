#include "SampleAnalyzer/User/Analyzer/new_smearer_reco.h"
#include "SampleAnalyzer/User/Analyzer/sigmas.h"
#include "SampleAnalyzer/User/Analyzer/reco.h"
using namespace MA5;

/// Tau smearing method
MCParticleFormat NewSmearer::TauSmearer(const MCParticleFormat * part)
{
    MCParticleFormat *Tau = &(output_);
    Tau->Reset();
      MAdouble64 acceptance =  fct_reco_bnd_15_1(part->pt())  *  fct_reco_15_1()  +  fct_reco_bnd_15_2(part->abseta(), part->pt())  *  fct_reco_15_2()  +  fct_reco_bnd_15_3(part->abseta(), part->pt())  *  fct_reco_15_3() ;
      if (RANDOM->flat() > acceptance)
      {
          return output_;
      }
    SetDefaultOutput(part, output_);
    MAdouble64 sigma =  fct_bnd_15_E_1(Tau->abseta())  *  fct_eff_15_E_1(Tau->e())  +  fct_bnd_15_E_2(Tau->abseta())  *  fct_eff_15_E_2(Tau->e())  +  fct_bnd_15_E_3(Tau->abseta())  *  fct_eff_15_E_3(Tau->e()) ;
    if ( sigma != 0. )
    {
      MAdouble64 smeared_object = Gaussian(sigma,Tau->e());
      if (smeared_object < 0.) smeared_object = 0.;
      Tau->momentum().SetPtEtaPhiE(smeared_object/cosh(Tau->eta()), Tau->eta(), Tau->phi(), smeared_object);
    }
    return output_;
}

/// Muon smearing method
MCParticleFormat NewSmearer::MuonSmearer(const MCParticleFormat * part)
{
    MCParticleFormat *Muon = &(output_);
    Muon->Reset();
      MAdouble64 acceptance =  fct_reco_bnd_13_1(part->pt(), part->abseta())  *  fct_reco_13_1()  +  fct_reco_bnd_13_2(part->pt(), part->abseta())  *  fct_reco_13_2()  +  fct_reco_bnd_13_3(part->pt(), part->abseta())  *  fct_reco_13_3() ;
      if (RANDOM->flat() > acceptance)
      {
          return output_;
      }
    SetDefaultOutput(part, output_);
    MAdouble64 sigma =  fct_bnd_13_PT_1(Muon->pt(), Muon->abseta())  *  fct_eff_13_PT_1(Muon->pt())  +  fct_bnd_13_PT_2(Muon->pt(), Muon->abseta())  *  fct_eff_13_PT_2(Muon->pt())  +  fct_bnd_13_PT_3(Muon->pt(), Muon->abseta())  *  fct_eff_13_PT_3(Muon->pt()) ;
    if ( sigma != 0. )
    {
      MAdouble64 smeared_object = Gaussian(sigma,Muon->pt());
      if (smeared_object < 0.) smeared_object = 0.;
      Muon->momentum().SetPtEtaPhiE(smeared_object, Muon->eta(), Muon->phi(), smeared_object*cosh(Muon->eta()));
    }
    return output_;
}

/// Electron smearing method
MCParticleFormat NewSmearer::ElectronSmearer(const MCParticleFormat * part)
{
    MCParticleFormat *Electron = &(output_);
    Electron->Reset();
      MAdouble64 acceptance =  fct_reco_bnd_11_1(part->abseta(), part->pt())  *  fct_reco_11_1()  +  fct_reco_bnd_11_2(part->pt(), part->abseta())  *  fct_reco_11_2()  +  fct_reco_bnd_11_3(part->pt(), part->abseta())  *  fct_reco_11_3() ;
      if (RANDOM->flat() > acceptance)
      {
          return output_;
      }
    SetDefaultOutput(part, output_);
    MAdouble64 sigma =  fct_bnd_11_PT_1(Electron->pt(), Electron->abseta())  *  fct_eff_11_PT_1(Electron->pt())  +  fct_bnd_11_PT_2(Electron->pt(), Electron->abseta())  *  fct_eff_11_PT_2(Electron->pt())  +  fct_bnd_11_PT_3(Electron->pt(), Electron->abseta())  *  fct_eff_11_PT_3(Electron->pt()) ;
    if ( sigma != 0. )
    {
      MAdouble64 smeared_object = Gaussian(sigma,Electron->pt());
      if (smeared_object < 0.) smeared_object = 0.;
      Electron->momentum().SetPtEtaPhiE(smeared_object, Electron->eta(), Electron->phi(), smeared_object*cosh(Electron->eta()));
    }
    sigma = 0.;
    sigma =  fct_bnd_11_E_1(Electron->abseta())  *  fct_eff_11_E_1(Electron->e())  +  fct_bnd_11_E_2(Electron->abseta())  *  fct_eff_11_E_2(Electron->e()) ;
    if ( sigma != 0. )
    {
      MAdouble64 smeared_object = Gaussian(sigma,Electron->e());
      if (smeared_object < 0.) smeared_object = 0.;
      Electron->momentum().SetPtEtaPhiE(smeared_object/cosh(Electron->eta()), Electron->eta(), Electron->phi(), smeared_object);
    }
    return output_;
}

