#ifndef EFF_H_INCLUDED
#define EFF_H_INCLUDED
#include <cmath>
#include <math.h>
#include <iostream>
MAdouble64 fct_eff_21_5_1(MAdouble64 PT)
{
   return (0.002 + (7.3e-06 * PT));
}

MAbool fct_bnd_21_5_1()
{
   return true;
}

MAdouble64 fct_eff_4_5_1(MAdouble64 PT)
{
   return ((0.2 * std::tanh((0.02 * PT))) * (1.0 / (1.0 + (0.0034 * PT))));
}

MAbool fct_bnd_4_5_1(MAdouble64 ABSETA)
{
   return (ABSETA < 2.5);
}

MAdouble64 fct_eff_4_5_2()
{
   return 0.0;
}

MAbool fct_bnd_4_5_2(MAdouble64 ABSETA)
{
   return (ABSETA >= 2.5);
}

MAdouble64 fct_eff_5_5_1(MAdouble64 PT)
{
   return ((0.8 * std::tanh((0.003 * PT))) * (30.0 / (1.0 + (0.086 * PT))));
}

MAbool fct_bnd_5_5_1(MAdouble64 ABSETA)
{
   return (ABSETA < 2.5);
}

MAdouble64 fct_eff_5_5_2()
{
   return 0.0;
}

MAbool fct_bnd_5_5_2(MAdouble64 ABSETA)
{
   return (ABSETA >= 2.5);
}

MAdouble64 fct_eff_15_15_1()
{
   return 0.6;
}

MAbool fct_bnd_15_15_1(MAdouble64 NTRACKS, MAdouble64 ABSETA)
{
   return ((ABSETA <= 2.5)  &&  (NTRACKS >= 2.0));
}

MAdouble64 fct_eff_15_15_2()
{
   return 0.7;
}

MAbool fct_bnd_15_15_2(MAdouble64 NTRACKS, MAdouble64 ABSETA)
{
   return ((ABSETA <= 2.5)  &&  (NTRACKS == 1.0));
}

MAdouble64 fct_eff_15_15_3()
{
   return 0.0;
}

MAbool fct_bnd_15_15_3(MAdouble64 ABSETA)
{
   return (ABSETA > 2.5);
}

#endif