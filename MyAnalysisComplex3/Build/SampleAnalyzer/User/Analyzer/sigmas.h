#ifndef SIG_H_INCLUDED
#define SIG_H_INCLUDED
#include <cmath>
#include <math.h>
#include <iostream>
MAdouble64 fct_eff_11_PT_1(MAdouble64 PT)
{
   return std::sqrt((pow(0.03, 2.0) + (pow(PT, 2.0) * pow(0.0013, 2.0))));
}

MAbool fct_bnd_11_PT_1(MAdouble64 PT, MAdouble64 ABSETA)
{
   return ((ABSETA <= 0.5)  &&  (PT > 0.1));
}

MAdouble64 fct_eff_11_PT_2(MAdouble64 PT)
{
   return std::sqrt((pow(0.05, 2.0) + (pow(PT, 2.0) * pow(0.0017, 2.0))));
}

MAbool fct_bnd_11_PT_2(MAdouble64 PT, MAdouble64 ABSETA)
{
   return (((ABSETA > 0.5)  &&  (ABSETA <= 1.5))  &&  (PT > 0.1));
}

MAdouble64 fct_eff_11_PT_3(MAdouble64 PT)
{
   return std::sqrt((pow(0.15, 2.0) + (pow(PT, 2.0) * pow(0.0031, 2.0))));
}

MAbool fct_bnd_11_PT_3(MAdouble64 PT, MAdouble64 ABSETA)
{
   return (((ABSETA > 1.5)  &&  (ABSETA <= 2.5))  &&  (PT > 0.1));
}

MAdouble64 fct_eff_11_E_1(MAdouble64 E)
{
   return std::sqrt(((pow(E, 2.0) * pow(0.0017, 2.0)) + (E * pow(0.101, 2.0))));
}

MAbool fct_bnd_11_E_1(MAdouble64 ABSETA)
{
   return (ABSETA <= 3.2);
}

MAdouble64 fct_eff_11_E_2(MAdouble64 E)
{
   return std::sqrt(((pow(E, 2.0) * pow(0.035, 2.0)) + (E * pow(0.285, 2.0))));
}

MAbool fct_bnd_11_E_2(MAdouble64 ABSETA)
{
   return ((ABSETA > 3.2)  &&  (ABSETA <= 4.9));
}

MAdouble64 fct_eff_13_PT_1(MAdouble64 PT)
{
   return std::sqrt((pow(0.01, 2.0) + (pow(PT, 2.0) * pow(0.0001, 2.0))));
}

MAbool fct_bnd_13_PT_1(MAdouble64 PT, MAdouble64 ABSETA)
{
   return ((ABSETA <= 0.5)  &&  (PT > 0.1));
}

MAdouble64 fct_eff_13_PT_2(MAdouble64 PT)
{
   return std::sqrt((pow(0.015, 2.0) + (pow(PT, 2.0) * pow(0.00015, 2.0))));
}

MAbool fct_bnd_13_PT_2(MAdouble64 PT, MAdouble64 ABSETA)
{
   return (((ABSETA > 0.5)  &&  (ABSETA <= 1.5))  &&  (PT > 0.1));
}

MAdouble64 fct_eff_13_PT_3(MAdouble64 PT)
{
   return std::sqrt((pow(0.025, 2.0) + (pow(PT, 2.0) * pow(0.00035, 2.0))));
}

MAbool fct_bnd_13_PT_3(MAdouble64 PT, MAdouble64 ABSETA)
{
   return (((ABSETA > 1.5)  &&  (ABSETA <= 2.5))  &&  (PT > 0.1));
}

MAdouble64 fct_eff_15_E_1(MAdouble64 E)
{
   return std::sqrt((((pow(E, 2.0) * pow(0.0302, 2.0)) + (E * pow(0.5205, 2.0))) + pow(1.59, 2.0)));
}

MAbool fct_bnd_15_E_1(MAdouble64 ABSETA)
{
   return (ABSETA <= 1.7);
}

MAdouble64 fct_eff_15_E_2(MAdouble64 E)
{
   return std::sqrt(((pow(E, 2.0) * pow(0.05, 2.0)) + (E * pow(0.706, 2.0))));
}

MAbool fct_bnd_15_E_2(MAdouble64 ABSETA)
{
   return ((ABSETA > 1.7)  &&  (ABSETA <= 3.2));
}

MAdouble64 fct_eff_15_E_3(MAdouble64 E)
{
   return std::sqrt(((pow(E, 2.0) * pow(0.0942, 2.0)) + E));
}

MAbool fct_bnd_15_E_3(MAdouble64 ABSETA)
{
   return ((ABSETA > 3.2)  &&  (ABSETA <= 4.9));
}

#endif