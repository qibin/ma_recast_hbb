#include "SampleAnalyzer/User/Analyzer/new_tagger.h"
#include "SampleAnalyzer/User/Analyzer/efficiencies.h"
#include "SampleAnalyzer/Commons/Service/RandomService.h"
#include "SampleAnalyzer/Commons/Service/PDGService.h"
#include "SampleAnalyzer/Commons/Service/Physics.h"
using namespace MA5;
#pragma message ( "TAGGER VERSION: " "1.5" )
void NewTagger::Execute(SampleFormat& sample, EventFormat& event)
{
  // Storing the IDs of objects that need to leave a collection
  std::vector<MAuint32> toRemove;

  // Shortcut for global event variables
  MAfloat64 & THT  = event.rec()->THT();
  MAfloat64 & Meff = event.rec()->Meff();

  // b/c-tagging + jet-mistagging
  MAuint32 Ntaus = event.rec()->taus().size();
  for (MAuint32 i=0; i<event.rec()->jets().size(); i++)
  {
    // We have a true b-jet: is it b-tagged?
    if (event.rec()->jets()[i].true_btag())
    {
      // MAdouble64  efficiency =  fct_bnd_5_5_1((&event.rec()->jets()[i])->abseta())  *  fct_eff_5_5_1((&event.rec()->jets()[i])->pt())  +  fct_bnd_5_5_2((&event.rec()->jets()[i])->abseta())  *  fct_eff_5_5_2() ;
      MAdouble64  efficiency = 1.00;
      if (RANDOM->flat() > efficiency) { (&event.rec()->jets()[i])->setBtag(false); }
      else{ (&event.rec()->jets()[i])->setBtag(true); }
    }

    // We have a true c-jet: is it b-tagged?
    else if (event.rec()->jets()[i].true_ctag())
    {
      MAdouble64  efficiency =  fct_bnd_4_5_1((&event.rec()->jets()[i])->abseta())  *  fct_eff_4_5_1((&event.rec()->jets()[i])->pt())  +  fct_bnd_4_5_2((&event.rec()->jets()[i])->abseta())  *  fct_eff_4_5_2() ;
      if (RANDOM->flat() < efficiency) { (&event.rec()->jets()[i])->setBtag(true); }
      else{(&event.rec()->jets()[i])->setBtag(false);}
    }

    // We have a true light-jet: is it b-tagged?
    else 
    {
      MAdouble64  efficiency =  fct_bnd_21_5_1()  *  fct_eff_21_5_1((&event.rec()->jets()[i])->pt()) ;
      if (RANDOM->flat() < efficiency) { (&event.rec()->jets()[i])->setBtag(true); }
      else{ (&event.rec()->jets()[i])->setBtag(false); }
    }

    // We have a b-tagged jet -> moving on with the next jet
    if (event.rec()->jets()[i].btag()) { continue; }

    // We have a c-tagged jet -> moving on with the next jet
    if (event.rec()->jets()[i].ctag()) { continue; }

  }

  for (MAuint32 i=toRemove.size();i>0;i--)
    event.rec()->jets().erase(event.rec()->jets().begin() + toRemove[i-1]);
  toRemove.clear();

  // tau-tagging
  for (MAuint32 i=0; i<Ntaus; i++)
  {
      MAdouble64  efficiency =  fct_bnd_15_15_1((&event.rec()->taus()[i])->ntracks(), (&event.rec()->taus()[i])->abseta())  *  fct_eff_15_15_1()  +  fct_bnd_15_15_2((&event.rec()->taus()[i])->ntracks(), (&event.rec()->taus()[i])->abseta())  *  fct_eff_15_15_2()  +  fct_bnd_15_15_3((&event.rec()->taus()[i])->abseta())  *  fct_eff_15_15_3() ;
      if (RANDOM->flat() > efficiency)
      {
        RecJetFormat* NewParticle = event.rec()->GetNewJet();
        NewParticle->setMomentum((&event.rec()->taus()[i])->momentum());
        NewParticle->setMc((&event.rec()->taus()[i])->mc());
        NewParticle->setNtracks((&event.rec()->taus()[i])->ntracks());
        toRemove.push_back(i);
        continue;
      }
  }
  for (MAuint32 i=toRemove.size();i>0;i--)
    event.rec()->taus().erase(event.rec()->taus().begin() + toRemove[i-1]);
  toRemove.clear();

}

void NewTagger::Execute(SampleFormat& sample, const EventFormat& event, std::vector<RecJetFormat>& jets)
{
  // Storing the IDs of objects that need to leave a collection
  // std::vector<MAuint32> toRemove;

  // Shortcut for global event variables
  const MAfloat64 & THT  = event.rec()->THT();
  const MAfloat64 & Meff = event.rec()->Meff();

  // b/c-tagging + jet-mistagging
  MAuint32 Ntaus = event.rec()->taus().size();
  for (MAuint32 i=0; i<jets.size(); i++)
  {
    // We have a true b-jet: is it b-tagged?
    if (jets[i].true_btag())
    {
      // MAdouble64  efficiency =  fct_bnd_5_5_1((&jets[i])->abseta())  *  fct_eff_5_5_1((&jets[i])->pt())  +  fct_bnd_5_5_2((&jets[i])->abseta())  *  fct_eff_5_5_2() ;
      MAdouble64  efficiency = 0.8;
      if (RANDOM->flat() > efficiency) { (&jets[i])->setBtag(false); }
      else{(&jets[i])->setBtag(true);}
    }

    // // We have a true c-jet: is it b-tagged?
    // else if (jets[i].true_ctag())
    // {
    //   MAdouble64  efficiency =  fct_bnd_4_5_1((&jets[i])->abseta())  *  fct_eff_4_5_1((&jets[i])->pt())  +  fct_bnd_4_5_2((&jets[i])->abseta())  *  fct_eff_4_5_2() ;
    //   if (RANDOM->flat() < efficiency) { (&jets[i])->setBtag(true); }
    //   else{(&jets[i])->setBtag(false);}
    // }

    // // We have a true light-jet: is it b-tagged?
    // else 
    // {
    //   MAdouble64  efficiency =  fct_bnd_21_5_1()  *  fct_eff_21_5_1((&jets[i])->pt()) ;
    //   if (RANDOM->flat() < efficiency) { (&jets[i])->setBtag(true); }
    //   else{(&jets[i])->setBtag(false);}
    // }

    // We have a b-tagged jet -> moving on with the next jet
    if (jets[i].btag()) { continue; }

    // We have a c-tagged jet -> moving on with the next jet
    if (jets[i].ctag()) { continue; }

  }

  // for (MAuint32 i=toRemove.size();i>0;i--)
  //   jets.erase(jets.begin() + toRemove[i-1]);
  // toRemove.clear();

}

