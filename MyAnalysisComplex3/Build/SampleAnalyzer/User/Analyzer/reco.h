#ifndef RECO_H_INCLUDED
#define RECO_H_INCLUDED
#include <cmath>
#include <math.h>
#include <iostream>
MAdouble64 fct_reco_11_1()
{
   return 0.0;
}

MAbool fct_reco_bnd_11_1(MAdouble64 ABSETA, MAdouble64 PT)
{
   return ((PT <= 10.0)  ||  (ABSETA > 2.5));
}

MAdouble64 fct_reco_11_2()
{
   return 0.9;
}

MAbool fct_reco_bnd_11_2(MAdouble64 PT, MAdouble64 ABSETA)
{
   return ((ABSETA <= 1.5)  &&  (PT > 10.0));
}

MAdouble64 fct_reco_11_3()
{
   return 0.7;
}

MAbool fct_reco_bnd_11_3(MAdouble64 PT, MAdouble64 ABSETA)
{
   return (((ABSETA > 1.5)  &&  (ABSETA <= 2.5))  &&  (PT > 10.0));
}

MAdouble64 fct_reco_13_1()
{
   return 0.0;
}

MAbool fct_reco_bnd_13_1(MAdouble64 PT, MAdouble64 ABSETA)
{
   return ((ABSETA > 2.5)  ||  (PT <= 10.0));
}

MAdouble64 fct_reco_13_2()
{
   return 0.95;
}

MAbool fct_reco_bnd_13_2(MAdouble64 PT, MAdouble64 ABSETA)
{
   return ((ABSETA <= 1.5)  &&  (PT > 10.0));
}

MAdouble64 fct_reco_13_3()
{
   return 0.85;
}

MAbool fct_reco_bnd_13_3(MAdouble64 PT, MAdouble64 ABSETA)
{
   return (((ABSETA > 1.5)  &&  (ABSETA < 2.5))  &&  (PT > 10.0));
}

MAdouble64 fct_reco_15_1()
{
   return 0.0;
}

MAbool fct_reco_bnd_15_1(MAdouble64 PT)
{
   return (PT <= 20.0);
}

MAdouble64 fct_reco_15_2()
{
   return 0.65;
}

MAbool fct_reco_bnd_15_2(MAdouble64 ABSETA, MAdouble64 PT)
{
   return ((PT > 20.0)  &&  (ABSETA <= 2.5));
}

MAdouble64 fct_reco_15_3()
{
   return 1.0;
}

MAbool fct_reco_bnd_15_3(MAdouble64 ABSETA, MAdouble64 PT)
{
   return ((PT > 20.0)  &&  (ABSETA > 2.5));
}

#endif