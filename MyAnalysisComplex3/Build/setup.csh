#!/bin/csh -f

# Defining colours for shell
set GREEN  = "\033[1;32m"
set RED    = "\033[1;31m"
set PINK   = "\033[1;35m"
set BLUE   = "\033[1;34m"
set YELLOW = "\033[1;33m"
set CYAN   = "\033[1;36m"
set NORMAL = "\033[0;39m"

# Configuring MA5 environment variable
setenv MA5_BASE /lustre/collider/liuqibin/MC/Sbb/localarea5/substructure

# Configuring PATH environment variable
if ( $?PATH ) then
setenv PATH $MA5_BASE/tools/SampleAnalyzer/ExternalSymLink/Bin:/cvmfs/sft.cern.ch/lcg/releases/ROOT/v6.20.02-d9e99/x86_64-slc6-gcc8-opt/bin:/cvmfs/sft.cern.ch/lcg/releases/fastjet/3.3.2-c962b/x86_64-slc6-gcc8-opt/bin:"$PATH"
else
setenv PATH $MA5_BASE/tools/SampleAnalyzer/ExternalSymLink/Bin:/cvmfs/sft.cern.ch/lcg/releases/ROOT/v6.20.02-d9e99/x86_64-slc6-gcc8-opt/bin:/cvmfs/sft.cern.ch/lcg/releases/fastjet/3.3.2-c962b/x86_64-slc6-gcc8-opt/bin
endif

# Configuring LD_LIBRARY_PATH environment variable
if ( $?LD_LIBRARY_PATH ) then
setenv LD_LIBRARY_PATH $MA5_BASE/tools/SampleAnalyzer/Lib:$MA5_BASE/tools/SampleAnalyzer/ExternalSymLink/Lib:/cvmfs/sft.cern.ch/lcg/releases/ROOT/v6.20.02-d9e99/x86_64-slc6-gcc8-opt/lib:/cvmfs/sft.cern.ch/lcg/releases/fastjet/3.3.2-c962b/x86_64-slc6-gcc8-opt/lib:/cvmfs/sft.cern.ch/lcg/releases/gcc/8.2.0-3fa06/x86_64-slc6/bin/../lib/gcc/x86_64-pc-linux-gnu/8.2.0:/cvmfs/sft.cern.ch/lcg/releases/gcc/8.2.0-3fa06/x86_64-slc6/bin/../lib/gcc:/cvmfs/sft.cern.ch/lcg/releases/gcc/8.2.0-3fa06/x86_64-slc6/bin/../lib/gcc/x86_64-pc-linux-gnu/8.2.0/../../../../lib64:/lib/../lib64:/usr/lib/../lib64:/cvmfs/sft.cern.ch/lcg/releases/gcc/8.2.0-3fa06/x86_64-slc6/bin/../lib/gcc/x86_64-pc-linux-gnu/8.2.0/../../..:"$LD_LIBRARY_PATH"
else
setenv LD_LIBRARY_PATH $MA5_BASE/tools/SampleAnalyzer/Lib:$MA5_BASE/tools/SampleAnalyzer/ExternalSymLink/Lib:/cvmfs/sft.cern.ch/lcg/releases/ROOT/v6.20.02-d9e99/x86_64-slc6-gcc8-opt/lib:/cvmfs/sft.cern.ch/lcg/releases/fastjet/3.3.2-c962b/x86_64-slc6-gcc8-opt/lib:/cvmfs/sft.cern.ch/lcg/releases/gcc/8.2.0-3fa06/x86_64-slc6/bin/../lib/gcc/x86_64-pc-linux-gnu/8.2.0:/cvmfs/sft.cern.ch/lcg/releases/gcc/8.2.0-3fa06/x86_64-slc6/bin/../lib/gcc:/cvmfs/sft.cern.ch/lcg/releases/gcc/8.2.0-3fa06/x86_64-slc6/bin/../lib/gcc/x86_64-pc-linux-gnu/8.2.0/../../../../lib64:/lib/../lib64:/usr/lib/../lib64:/cvmfs/sft.cern.ch/lcg/releases/gcc/8.2.0-3fa06/x86_64-slc6/bin/../lib/gcc/x86_64-pc-linux-gnu/8.2.0/../../..
endif

# Checking that all environment variables are defined
if ( $?MA5_BASE && $?PATH && $?LD_LIBRARY_PATH ) then
echo $YELLOW"--------------------------------------------------------"
echo "    Your environment is properly configured for MA5     "
echo "--------------------------------------------------------"$NORMAL
endif
