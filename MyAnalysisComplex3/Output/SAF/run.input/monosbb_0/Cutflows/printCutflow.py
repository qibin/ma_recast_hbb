#!/usr/bin/python
import xml.etree.ElementTree as ET

def parseCutflow(filename):
    xmlstr = '<?xml version="1.0"?><data>'
    with open(filename) as f:
        xmlstr += f.read()
    xmlstr += "</data>"
    xmlstr = xmlstr.replace("&","and")
    try:
        parsed=ET.fromstring(xmlstr)
    except Exception:
        ValueError("Failed to parse SAF file as XML")
    cutflow_name=[]
    cutflow_value=[]
    initial=parsed.find("InitialCounter")
    trig=False
    for l in initial.text.split("\n"):
        if not trig:
            if "Initial number of events" in l:
                cutflow_name.append("Initial")
                trig=True
                cutflow_value.append([])
        else:
            splitted=l.split()
	    #print(splitted)
            if len(splitted)<3:
                continue
            cutflow_value[-1]=cutflow_value[-1]+[float(splitted[0])]
    for c in parsed.findall("Counter"):
        trig=False
        for l in c.text.split("\n"):
            splitted=l.split()
	    #print(splitted)
            if len(splitted)<3:
                continue
            if not trig:
                if splitted[0].startswith('"'):
                    cutflow_name.append(splitted[0].strip('"'))
                    trig=True
                    cutflow_value.append([])
            else:
	        cutflow_value[-1]=cutflow_value[-1]+[float(splitted[0])]
    return cutflow_name,cutflow_value

def printCutflow(names,values):
    for name,[evt,sumw,sumw2] in zip(names,values):
        print("{}\t{:.0f}\t{:.6e}".format(name,evt,sumw))

if __name__ == "__main__":
    import argparse
    import os

    # Python2 by default evaluates the result from `input()`, Python3
    # takes it as a string. Enforce this also for Python2.
    try:
        input = raw_input
    except NameError:
        pass

    parser = argparse.ArgumentParser()
    helptext = 'One or more SAF files. Separate multiple files with spaces'
    parser.add_argument('-f', '--files', type=str, nargs="+",
                        help=helptext, required=True)
    
    args = parser.parse_args()

    # iterate over all SAF files provided as command line arguments
    for saffile in args.files:
        # read and parse SAF file
        print("========",saffile,"========")
        p=parseCutflow(saffile)
        #print(p)
        printCutflow(*p)


           